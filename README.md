# Terrain simplification #

Copyright 2020 TU Graz

### Author ###

Michael Kerber

### Summary ###

This software is an implementation of the algorithm described in the paper

Ulderico Fugacci, Michael Kerber, Hugo Manet: Topology-Preserving Terrain Simplification, Accepted for SIGSPATIAL 2020. Also available at [arxiv 1912.03032](https://arxiv.org/abs/1912.03032).

The software accepts a text file representing a terrain and outputs a geometrically and topologically simplified terrain with fewer vertices.

### Requirements ###

The code makes relies heavily on the CGAL library. It was tested using version 4.14.

It also requires the libraries Timer and Program_options of the boost library. It was tested using version 1.62.

A CMakeLists.txt sample file is included. Ideally, the program "simplifier" should be created by

	cmake .
	make

In many cases, it might be necessary to specify the path of the CGAL library,
for instance

	cmake -DCGAL_DIR=/path/to/cgal .
	make

### Usage ###

After compilation, the program simplifier expects an input text file. Every line is interpreted as a point, and should consist of three values, where the first and second are the coordinates of the point, and the 3rd is the elevation of the point.
The terrain is then defined as the Delaunay triangulation of the points with linear interpolation of the elevation. The output is an obj-file, representing the simplified output terrain.

The program offers various options which can be displayed by

	./simplifier --help

The meaning of the various options is described in the paper above.

### Important ###

The algorithm might not work if the delta value chosen happen to coincide with the difference of two elevations. In this case, the error message

"A bad delta is chosen: the blw algorithm cannot find a subdivision point that satisfies the L_infty constraint. Please try with another delta"

Note that this is a degenerate case and can be avoided by a slight perturbation of delta (e.g., choosing 4.0001 instead of 4).

### License ###

The software is published under the GNU Lesser General Public License (LGPL).

### Contact ###

Michael Kerber (kerber@tugraz.at)