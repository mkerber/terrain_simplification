/*
 * Copyright 2020 TU Graz
 * Author: Michael Kerber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <CGAL/Cartesian.h>
#include <CGAL/Arrangement_with_history_2.h>
#include <CGAL/Arr_segment_traits_2.h>
#include <CGAL/Arr_extended_dcel.h>
#include <CGAL/Arr_naive_point_location.h>
#include <CGAL/Arr_trapezoid_ric_point_location.h>
#include <CGAL/Arr_overlay_2.h>
#include <CGAL/Arr_accessor.h>
#include <CGAL/Arrangement_2/Arr_traits_adaptor_2.h>
#include <CGAL/Arr_default_overlay_traits.h>
#include <CGAL/Delaunay_triangulation_2.h>
#include <CGAL/utility.h>
#include <CGAL/Union_find.h>
#include <vector>
#include <fstream>
#include <queue>
#include <unordered_map>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/reverse_graph.hpp>
#include <boost/graph/copy.hpp>

#include <boost/timer/timer.hpp>

#include <boost/program_options.hpp>

boost::timer::cpu_timer overall_timer, algorithm_timer, create_arrangement_timer, blw_timer, reduction_timer,blw_pure_timer,convert_to_pl_timer,L_infty_timer,get_inter_timer,ch_tri_timer,range_search_timer,test_timer1,test_timer2,test_timer3,test_timer4,flip_timer, progress_timer;

void initialize_timers() {
  overall_timer.start();
  algorithm_timer.start();
  algorithm_timer.stop();
  create_arrangement_timer.start();
  create_arrangement_timer.stop();
  blw_timer.start();
  blw_timer.stop();
  blw_pure_timer.start();
  blw_pure_timer.stop();
  convert_to_pl_timer.start();
  convert_to_pl_timer.stop();
  reduction_timer.start();
  reduction_timer.stop();
  L_infty_timer.start();
  L_infty_timer.stop();
  get_inter_timer.start();
  get_inter_timer.stop();
  ch_tri_timer.start();
  ch_tri_timer.stop();
  flip_timer.start();
  flip_timer.stop();
  progress_timer.start();
  progress_timer.stop();
  test_timer1.start();
  test_timer1.stop();
  test_timer2.start();
  test_timer2.stop();
  test_timer3.start();
  test_timer3.stop();
  test_timer4.start();
  test_timer4.stop();
  
  range_search_timer.start();
  range_search_timer.stop();
}

void print_timers() {
  overall_timer.stop();
  std::cout << "Overall timer: " << double(overall_timer.elapsed().wall)/std::pow(10,9) << std::endl;
  std::cout << "Overall timer (user): " << double(overall_timer.elapsed().user)/std::pow(10,9) << std::endl;
  std::cout << "Algorithm timer: " << double(algorithm_timer.elapsed().wall)/std::pow(10,9) << std::endl;
  std::cout << "Algorithm timer (user): " << double(algorithm_timer.elapsed().user)/std::pow(10,9) << std::endl;
  std::cout << "Create arrangement timer: " << double(create_arrangement_timer.elapsed().wall)/std::pow(10,9) << "  " << double(create_arrangement_timer.elapsed().wall)/double(algorithm_timer.elapsed().wall)*100 << "%" << std::endl;
  std::cout << "BLW timer: " << double(blw_timer.elapsed().wall)/std::pow(10,9) << "  " << double(blw_timer.elapsed().wall)/double(algorithm_timer.elapsed().wall)*100 << "%" << std::endl;
  std::cout << "Reduction timer: " << double(reduction_timer.elapsed().wall)/std::pow(10,9) << "  " << double(reduction_timer.elapsed().wall)/double(algorithm_timer.elapsed().wall)*100 << "%" << std::endl;
  std::cout << "Edge flip timer: " << double(flip_timer.elapsed().wall)/std::pow(10,9) <<  "  " << double(flip_timer.elapsed().wall)/double(algorithm_timer.elapsed().wall)*100<< "%" << std::endl;
  std::cout << "---" << std::endl;
  std::cout << "BLW (computing Morse function) timer: " << double(blw_pure_timer.elapsed().wall)/std::pow(10,9) << std::endl;
  std::cout << "BLW (computing PL function) timer: " << double(convert_to_pl_timer.elapsed().wall)/std::pow(10,9) << std::endl;
  std::cout << "L_infty timer: " << double(L_infty_timer.elapsed().wall)/std::pow(10,9) << std::endl;
  std::cout << "get_inter timer: " << double(get_inter_timer.elapsed().wall)/std::pow(10,9) << std::endl;
  std::cout << "check triangles: " << double(ch_tri_timer.elapsed().wall)/std::pow(10,9) << std::endl;
  std::cout << "range_search: " << double(range_search_timer.elapsed().wall)/std::pow(10,9) << std::endl;
  std::cout << "test timer1: " << double(test_timer1.elapsed().wall)/std::pow(10,9) << std::endl;
  std::cout << "test timer2: " << double(test_timer2.elapsed().wall)/std::pow(10,9) << std::endl;
  std::cout << "test timer3: " << double(test_timer3.elapsed().wall)/std::pow(10,9) << std::endl;
  std::cout << "test timer4: " << double(test_timer4.elapsed().wall)/std::pow(10,9) << std::endl;
}


typedef CGAL::Exact_predicates_exact_constructions_kernel Kernel;
//typedef CGAL::Cartesian<double> Kernel;

typedef Kernel::FT NT;

typedef Kernel::Construct_segment_2 Construct_segment;

struct Data {

  NT val;
  CGAL::Object hint;
  bool visited;

  Data(NT val=0, CGAL::Object hint=CGAL::Object(), bool visited=false) : val(val), hint(hint),visited(visited) {}

  Data(const Data& data) : val(data.val), hint(data.hint), visited(data.visited) {}

};

typedef CGAL::Delaunay_triangulation_2<Kernel> Delaunay_triangulation;

typedef CGAL::Arr_segment_traits_2<Kernel> Arr_traits;
typedef Arr_traits::Point_2 Point;
typedef Arr_traits::X_monotone_curve_2 Segment;
typedef CGAL::Arr_extended_dcel<Arr_traits,Data,Data,Data> Dcel;
typedef CGAL::Arrangement_with_history_2<Arr_traits,Dcel> Arrangement;
typedef Arrangement::Vertex Vertex;
typedef Arrangement::Vertex_handle Vertex_handle;
typedef Arrangement::Vertex_const_handle Vertex_const_handle;
typedef Arrangement::Halfedge_handle Halfedge_handle;
typedef Arrangement::Halfedge_const_handle Halfedge_const_handle;
typedef Arrangement::Face_handle Face_handle;
typedef Arrangement::Face_const_handle Face_const_handle;
typedef Arrangement::Curve_handle Curve_handle;
typedef Arrangement::Halfedge_around_vertex_circulator Halfedge_circulator;

typedef CGAL::Arr_trapezoid_ric_point_location<Arrangement> Ric_point_location;

typedef CGAL::Triple<Point,Vertex_handle,Vertex_handle> Point_triple;

typedef std::pair<Vertex_handle,Vertex_handle> Vertex_pair;

bool verbose=false;

namespace CGAL {

template <typename OutputIterator>
OutputIterator
zone_with_hint(Arrangement& arr,
	       Segment& c,
	       OutputIterator oi,
	       CGAL::Object& hint)
{
  typedef Arr_compute_zone_visitor<Arrangement, OutputIterator> Zone_visitor;

  Zone_visitor visitor(oi);
  Arrangement_zone_2<Arrangement, Zone_visitor> arr_zone(arr, &visitor);

  arr_zone.init_with_hint(c, hint);
  arr_zone.compute_zone();

  return oi;
}

}

bool is_at_boundary(Vertex_handle v) {
  Arrangement::Halfedge_around_vertex_circulator circ=v->incident_halfedges(),
    start=circ;
  do {
    Face_handle f = circ->face();
    if(f->is_unbounded()) {
      return true;
    }
    circ++;
  } while(circ!=start);
  return false;
}

bool is_regular(Vertex_handle v,bool verbose_flag=false) {

  bool at_bd = is_at_boundary(v);
  //std::cout << "Checking whether " << v->point().x() << " " << v->point().y() << " is regular; Elevation=" << v->data().val << ", degree=" << v->degree() << std::endl;
  CGAL_assertion(at_bd || v->degree()>=3);

  NT vdata=v->data().val;

  Arrangement::Halfedge_around_vertex_circulator circ=v->incident_halfedges(),
    start=circ, next=circ,end;
  // First, try to find a position with a distinct data value
  next++;
  while(next!=start && next->source()->data().val==v->data().val) {
    next++;
  }
  if(next->source()->data().val==v->data().val) {
    // All data values are equal, we call this regular
    //if(verbose) std::cout << "All values equal for " << v->point() << std::endl;
    return true;
  }
  start=next;
  if(at_bd) {
    while(!start->face()->is_unbounded()) {
      start++;
    }
    end=start;
    start++;
    next=start;
  } else {
    end=start;
  }
  bool has_equal_data_neighbor=false;
  while(next->source()->data().val==v->data().val) {
    has_equal_data_neighbor=true;
    next++;
  }
  bool last_above = (start->source()->data().val>v->data().val);
  int number_of_side_changes=0;
  do {
    next++;
    Vertex_handle b = next->source();
    if(last_above && next->source()->data().val<v->data().val) {
      number_of_side_changes++;
      last_above=false;
    }
    if(!last_above && next->source()->data().val>v->data().val) {
      number_of_side_changes++;
      last_above=true;
    }
    if(next->source()->data().val==v->data().val) {
      has_equal_data_neighbor=true;
    }
  } while(next!=end);
  //if(verbose) std::cout << "Number of side changes: " << number_of_side_changes << std::endl;
  CGAL_assertion(at_bd || number_of_side_changes%2==0);
  ////if(verbose) std::cout << "Obtained " << number_of_side_changes << " side changes" << std::endl;
  return !at_bd ? (number_of_side_changes==2) || (number_of_side_changes==0 && has_equal_data_neighbor)
    : (number_of_side_changes==1) || (number_of_side_changes==0 && has_equal_data_neighbor);
}

int count_critical_points(Arrangement& arr, bool verbose_flag=false) {
  int count=0;
  for(Arrangement::Vertex_iterator vit = arr.vertices_begin();
      vit!=arr.vertices_end();
      vit++) {
    if(!is_regular(vit)) {
      if(verbose_flag) {
	std::cout << "Non-reg vertex " << vit->point() << " val: " << vit->data().val << " on bd: " << is_at_boundary(vit) << std::endl;
	Arrangement::Halfedge_around_vertex_circulator circ=vit->incident_halfedges(),start=circ;
	if(is_at_boundary(vit)) {
	  while(!start->face()->is_unbounded()) {
	    start++;
	  }
	  start++;
	  circ=start;
	}
	std::cout << "Nbhd: " << std::endl;
	do {
	  std::cout << circ->source()->data().val << " ";
	  circ++;
	} while(circ!=start);
	std::cout << std::endl;
      }
      count++;
    }
  }
  return count;
}

void get_intersections_with_line_segment(Arrangement& arr, 
					 Vertex_handle vp,
					 Vertex_handle vq,
					 std::vector<Point_triple>& intersection_with_segments,
					 std::vector<Vertex_handle>& vertices_encountered,
					 CGAL::Object& feature_at_p,
					 CGAL::Object& feature_at_q) {
  //get_inter_timer.resume();
  //if(verbose) std::cout << "New version of Get Inter With line seg " << vp->point() << " - " << vq->point() << std::endl;
  std::vector<CGAL::Object> objects;
  Arr_traits::Construct_min_vertex_2 min_vertex=arr.traits()->construct_min_vertex_2_object();

  intersection_with_segments.clear();
  vertices_encountered.clear();
  
  Segment seg = Construct_segment()(vp->point(),vq->point());

  Point min_point=min_vertex(seg);
  bool p_is_min = (min_point==vp->point());

  CGAL::Object& hint = p_is_min ? vp->data().hint : vq->data().hint;


  CGAL_assertion(hint.is<Vertex_const_handle>() || (hint.is<Halfedge_const_handle>()) || hint.is<Face_const_handle>());

  //CGAL::zone(arr, seg, std::back_inserter(objects), point_location);
  CGAL::zone_with_hint(arr, seg, std::back_inserter(objects), hint);

  //std::cout << "Found " << objects.size() << " features" << std::endl;

  int obj_size = objects.size();

  feature_at_p = (p_is_min ? objects[0] : objects[obj_size-1]);
  feature_at_q = (p_is_min ? objects[obj_size-1] : objects[0]);

  Vertex_handle v;
  Halfedge_handle e;
  Face_handle f;

  Arr_traits::Intersect_2 intersect = arr.traits()->intersect_2_object();  
  for(int i=1;i<objects.size()-1;i++) {
    CGAL::Object& obj = objects[i];
    if(CGAL::assign(v,obj)) {
      //std::cout << "Vertex" << v->point() << std::endl;
      vertices_encountered.push_back(v);
    }
    if(CGAL::assign(e,obj)) {
      //std::cout << "Edge" << e->source()->point() << " - " << e->target()->point() << std::endl;
      std::vector<CGAL::Object> intersections_of_two_segments;
      intersect(seg,e->curve(),std::back_inserter(intersections_of_two_segments));
      CGAL_assertion(intersections_of_two_segments.size()==1);
      std::pair<Point,Arr_traits::Multiplicity> result;
      if(CGAL::assign(result,intersections_of_two_segments[0])) {
	intersection_with_segments.push_back(Point_triple(result.first,e->source(),e->target()));
      } // Otherwise, it is an overlap, which we can ignore
    }
    if(CGAL::assign(f,obj)) {
      /*
      std::cout << "Face ";
      Arrangement::Ccb_halfedge_circulator run=f->outer_ccb();
      std::cout << run->source()->point() << " - ";
      run++;
      std::cout << run->source()->point() << " - ";
      run++;
      std::cout << run->source()->point() << std::endl;
      */
      // Do nothing
    }
  }
  //get_inter_timer.stop();

}


void set_lower_star_data(Arrangement& terrain) {
  for(Arrangement::Edge_iterator eit = terrain.edges_begin();
      eit!=terrain.edges_end();
      eit++) {
    eit->set_data(std::max(eit->source()->data().val,eit->target()->data().val));
    eit->twin()->set_data(eit->data());
  }
  for(Arrangement::Face_iterator fit = terrain.faces_begin();
      fit!=terrain.faces_end();
      fit++) {
    if(fit->is_unbounded()) {
      fit->set_data(NT(std::numeric_limits<double>::max()));
    } else {
      Arrangement::Ccb_halfedge_circulator run=fit->outer_ccb();
      NT val = run->source()->data().val;
      run++;
      val = std::max(val,run->source()->data().val);
      run++;
      val = std::max(val,run->source()->data().val);
      fit->set_data(val);
    }
  }
}

// It is assumed that the data objects where already created
void set_hints(Arrangement& terrain) {
  //if(verbose) std::cout << "Setting hints" << std::endl;
  for(Arrangement::Vertex_iterator vit = terrain.vertices_begin();
      vit!=terrain.vertices_end();
      vit++) {
    vit->data().hint=CGAL::make_object(Vertex_const_handle(vit));
    CGAL_assertion(vit->data().hint.is<Vertex_const_handle>());
  }
  for(Arrangement::Halfedge_iterator eit = terrain.halfedges_begin();
      eit!=terrain.halfedges_end();
      eit++) {
    eit->data().hint=CGAL::make_object(static_cast<Halfedge_const_handle>(eit));
  }
  for(Arrangement::Face_iterator fit = terrain.faces_begin();
      fit!=terrain.faces_end();
      fit++) {
    fit->data().hint = CGAL::make_object(Face_const_handle(fit));
  }
}



// The value of a point p that lies on the line segment a,b
NT _elevation_on_edge(const NT& px,const NT& py,
		      const NT& ax, const NT& ay,
		      const NT& bx, const NT& by,
		      const NT& elev_a,const NT& elev_b) {
  NT lambda;
  if(CGAL::abs(bx-ax)>CGAL::abs(by-ay)) {
    lambda = (px-ax)/(bx-ax);
  } else {
    lambda = (py-ay)/(by-ay);
  }
  return (1-lambda)*elev_a + lambda*elev_b;
}

const NT& cast (const Kernel::FT& x) {
  // Since NT==Kernel::FT, notyhing to be done
  return x;
}


NT elevation_on_edge(Point& p,
			 Vertex_handle va,
			 Vertex_handle vb) {
  
  return _elevation_on_edge(cast(p.x()),
			    cast(p.y()),
			    cast(va->point().x()),
			    cast(va->point().y()),
			    cast(vb->point().x()),
			    cast(vb->point().y()),
			    va->data().val,
			    vb->data().val);
}


NT dot(NT ux,NT uy ,NT vx, NT vy) {
  return ux*vx+uy*vy;
}

inline NT _elevation_in_triangle(const NT& px,const NT& py,
			  const NT& ax,const NT& ay,
			  const NT& bx,const NT& by,
			  const NT& cx,const NT& cy,
			  const NT& elev_a, const NT& elev_b, const NT& elev_c) {
  // https://math.stackexchange.com/questions/1887215/fast-way-of-computing-barycentric-coordinates-explained

  NT d00=dot(bx-ax,by-ay,bx-ax,by-ay);
  NT d01=dot(bx-ax,by-ay,cx-ax,cy-ay);
  NT d11=dot(cx-ax,cy-ay,cx-ax,cy-ay);
  NT d02=dot(bx-ax,by-ay,px-ax,py-ay);
  NT d12=dot(cx-ax,cy-ay,px-ax,py-ay);
  NT denom = d00*d11-d01*d01;
  NT v = (d11*d02-d01*d12)/denom;
  NT w = (d00*d12-d01*d02)/denom;
  NT u = NT(1.0)-v-w;
  //std::cout << "Bary: " << u << " " << v << " " << w << std::endl;
  return u*elev_a+v*elev_b+w*elev_c;
}

inline NT elevation_in_triangle(Point& p,
			 Vertex_handle& a,
			 Vertex_handle& b,
			 Vertex_handle& c) {
  //test_timer2.resume();
  NT res=_elevation_in_triangle(cast(p.x()),
				cast(p.y()),
				cast(a->point().x()),
				cast(a->point().y()),
				cast(b->point().x()),
				cast(b->point().y()),
				cast(c->point().x()),
				cast(c->point().y()),
				a->data().val,
				b->data().val,
				c->data().val);
  //test_timer2.stop();
  return res;
}

NT get_elevation_of_point_in_feature(Point& p,
				     CGAL::Object& object) {
  Vertex_handle v;
  Halfedge_handle e;
  Face_handle f;
  if(CGAL::assign(v,object)) {
    //if(verbose) std::cout << "Feature is vertex " << v->point() << ", data: " << v->data().val << std::endl;
    return v->data().val;
  }
  if(CGAL::assign(e,object)) {
    //if(verbose) std::cout << "Feature is edge " << e->source()->point() << " - " << e->target()->point() << std::endl;
    return elevation_on_edge(p,e->source(),e->target());
    }
  if(CGAL::assign(f,object)) {
    
    Arrangement::Ccb_halfedge_circulator run=f->outer_ccb();
    Vertex_handle a =run->source();
    run++;
    Vertex_handle b =run->source();
    run++;
    Vertex_handle c =run->source();
    //if(verbose) std::cout << "Feature is tri " << a->point() << " - " << b->point() << " - " << c->point() << std::endl;
    return elevation_in_triangle(p,a,b,c);
  }
  CGAL_assertion(false);
  return NT(0);
}

		     

NT compute_L_infty_distance_to(Vertex_handle vp,Vertex_handle vq,Arrangement& base_terrain) {
  //L_infty_timer.resume();
  //if(verbose) std::cout << "L_infty distance" << std::endl;
  //if(verbose) std::cout << "Elevation of " << vp->point().x() << " " << vp->point().y() << " : " << vp->data().val << std::endl;
  //if(verbose) std::cout << "Elevation of " << vq->point().x() << " " << vq->point().y() << " : " << vq->data().val << std::endl;
  std::vector<Point_triple> intersections;
  std::vector<Vertex_handle> vertices_encountered;
  CGAL::Object feature_at_vp,feature_at_vq;
  //get_inter_timer.resume();
  get_intersections_with_line_segment(base_terrain,
				      vp,
				      vq,
				      intersections,
				      vertices_encountered,
				      feature_at_vp,
				      feature_at_vq);
  //get_inter_timer.stop();
  // Only valid for generic input
  //CGAL_assertion(vertices_encountered.size()==0);
  //CGAL_assertion_code(Vertex_handle vh);
  //CGAL_assertion(CGAL::assign(vh,feature_at_vp));
  //CGAL_assertion(vh->point()==vp->point());
  //CGAL_assertion(CGAL::assign(vh,feature_at_vq));
  //CGAL_assertion(vh->point()==vq->point());
  
  //if(verbose) std::cout << "Found " << intersections.size() << " intersections" << std::endl;
  NT max_diff=0.0;
  
  // First check the elevation difference at the intersection points
  for(int i=0;i<intersections.size();i++) {
    Point r = intersections[i].first;
    Vertex_handle va = intersections[i].second;
    Vertex_handle vb = intersections[i].third;
    //Compute elevation in base terrain:
    NT elev_a=va->data().val;
    NT elev_b=vb->data().val;
    //if(verbose) std::cout << "Intersection Info:\n Intersection point: " << r << "\n base terrain a: " << va->point() << ", Elevation: " << elev_a << "\n base terrain b: " << vb->point() << ", Elevation: " << elev_b << std::endl;
    
    NT elev_point_on_base_terrain = elevation_on_edge(r,va,vb);
    //if(verbose) std::cout << "Elevation in base terrain: " << elev_point_on_base_terrain << std::endl;
    NT elev_point_on_simplified_terrain= elevation_on_edge(r,vp,vq);
    //if(verbose) std::cout << "Elevation in simplified terrain: " << elev_point_on_simplified_terrain << std::endl;
    NT diff = CGAL::abs(elev_point_on_base_terrain-elev_point_on_simplified_terrain);
    //if(verbose) std::cout << "Difference: " << diff << std::endl;
    if(diff>max_diff) {
      max_diff=diff;
    }
  }
  //if(verbose) std::cout << "Encountered " << vertices_encountered.size() << " vertices" << std::endl;
  // Now check the difference at encountered vertices of the base terrain
  for(int i=0;i<vertices_encountered.size();i++) {
    Vertex_handle vh = vertices_encountered[i];
    NT elev_point_on_base_terrain = vh->data().val;
    NT elev_point_on_simplified_terrain= elevation_on_edge(vh->point(),vp,vq);
    NT diff = CGAL::abs(elev_point_on_base_terrain-elev_point_on_simplified_terrain);
    //if(verbose) std::cout << "Diff at encountered " << i << ": " << diff << std::endl;
    //CGAL_assertion(CGAL::is_zero(diff));
    if(diff>max_diff) {
      max_diff=diff;
    }
  }
  
  // Check at vp 
  {
    NT elev_point_on_simplified_terrain = vp->data().val;
    NT elev_point_on_base_terrain=get_elevation_of_point_in_feature(vp->point(),feature_at_vp);
    //test_timer4.resume();
    NT diff = CGAL::abs(elev_point_on_base_terrain-elev_point_on_simplified_terrain);
    //if(verbose) std::cout << "Diff at vp " << vp->point() << ", data: " << vp->data().val << ": " << diff << std::endl;
    if(diff>max_diff) {
      max_diff=diff;
    }
    //test_timer4.stop();
  }
  // ..and at vq
  { 
    NT elev_point_on_simplified_terrain = vq->data().val;
    NT elev_point_on_base_terrain=get_elevation_of_point_in_feature(vq->point(),feature_at_vq);
    //test_timer4.resume();
    NT diff = CGAL::abs(elev_point_on_base_terrain-elev_point_on_simplified_terrain);
    //if(verbose) std::cout << "Diff at vq " << vq->point() << ", data: " << vq->data().val << ": " << diff << std::endl;
    if(diff>max_diff) {
      max_diff=diff;
    }
    //test_timer4.stop();
  }
  
  //if(verbose) std::cout << "max diff: " << max_diff << std::endl;
  //L_infty_timer.stop();
  return max_diff;
}

void triangular_range_search(Arrangement& arr,
			     Vertex_handle& va,
			     Vertex_handle& vb,
			     Vertex_handle& vc,
			     std::vector<Vertex_handle>& result) {

  Kernel::Triangle_2 tri(va->point(),vb->point(),vc->point());
  result.clear();
  std::vector<Point_triple> intersection_with_segments;
  std::vector<Vertex_handle> vertices_encountered;
  CGAL::Object feature_at_x, feature_at_y;
  
  std::vector<std::pair<Vertex_handle,Vertex_handle>> sides;
  sides.push_back(std::make_pair(va,vb));
  sides.push_back(std::make_pair(vb,vc));
  sides.push_back(std::make_pair(vc,va));

  std::queue<Vertex_handle> vertex_queue;

  for(int i=0;i<3;i++) {
    Vertex_handle x=sides[i].first;
    Vertex_handle y=sides[i].second;
    get_intersections_with_line_segment(arr, 
					x,
					y,
					intersection_with_segments,
					vertices_encountered,
					feature_at_x,
					feature_at_y);
    {
      // First check the feature at x and add it to one of the previous
      // vectors (makes the code shorter)
      Vertex_handle v;
      Halfedge_handle e;
      Face_handle f;
      if(CGAL::assign(v,feature_at_x)) {
	//if(verbose) std::cout << "Feature is vertex " << v->point() << ", data: " << v->data().val << std::endl;
	vertices_encountered.push_back(v);
      }
      if(CGAL::assign(e,feature_at_x)) {
	intersection_with_segments.push_back(Point_triple(x->point(),e->source(),e->target()));
      }
      if(CGAL::assign(f,feature_at_x)) {
	// do nothing
      }
    }
    for(int i = 0; i< intersection_with_segments.size(); i++) {
      Point_triple& triple = intersection_with_segments[i];
      Vertex_handle& v = triple.second;
      Vertex_handle& w = triple.third;
      if(! v->data().visited && tri.bounded_side(v->point())==CGAL::ON_BOUNDED_SIDE) {
	v->data().visited=true;
	vertex_queue.push(v);
	result.push_back(v);
      }
      if(! w->data().visited && tri.bounded_side(w->point())==CGAL::ON_BOUNDED_SIDE) {
	w->data().visited=true;
	vertex_queue.push(w);
	result.push_back(w);
      }
    }
    for(int i = 0; i< vertices_encountered.size();i++) {
      Vertex_handle& vertex_on_bd = vertices_encountered[i];
      vertex_queue.push(vertex_on_bd);
      // If only points inside the triangle should be returned, comment out the next two lines
      //vertex_on_bd->data().visited=true;
      //result.push_back(vertex_on_bd);
    }
  }
  // Now we need to examine the neighbors of vertices in the queue
  while(! vertex_queue.empty()) {
    Vertex_handle v = vertex_queue.front();
    vertex_queue.pop();
    Arrangement::Halfedge_around_vertex_circulator circ=v->incident_halfedges(),
      start=circ;
    
    do {
      CGAL_assertion(circ->target()==v);
      Vertex_handle w=circ->source();
      if(! w->data().visited && tri.bounded_side(w->point())==CGAL::ON_BOUNDED_SIDE) {
	w->data().visited=true;
	vertex_queue.push(w);
	result.push_back(w);
      }
      circ++;
    } while(circ!=start);
  }
  // Finally, reset the visited flag for all vertices
  for(int i=0;i<result.size();i++) {
    result[i]->data().visited=false;
  }
}		     



class Link_retriangulator {
  

  struct Edge_info {
    bool is_computed;
    bool is_geometry_aware;
    bool is_persistent_aware;
    bool dist_too_large;
    NT L_infty_dist;
    Edge_info() : is_computed(false) {}

    bool is_usable() {
      CGAL_assertion(is_computed);
      return is_geometry_aware && is_persistent_aware && !dist_too_large;
    }
  };

  struct Range_info {
    bool is_computed;
    bool is_triangulable;
    NT L_infty_bound;
    int index_of_range;

    Range_info() : is_computed(false) {}
  };

protected:
  Arrangement local_arr;

  bool _is_strictly_convex;
  std::vector<Vertex_handle> vertices_in_local_arr;
  std::vector<Vertex_handle> vertices_in_base_arr;
  Vertex_handle curr_handle;

  Arrangement& base_terrain;

  bool mind_persistent_aware;
  bool keep_critical_points; 
  bool mind_L_infty_on_edges;
  bool mind_L_infty_on_triangles;

  bool L_infty_threshold_set;
  NT L_infty_threshold;

  Construct_segment construct_segment;

  std::vector<std::vector<Edge_info> > _edge_infos;

  std::vector<std::vector<Range_info> > _range_infos;

  Range_info& get_range_info(int i, int j) {
    //if(verbose) std::cout << "Get_range_info: " << i << " " << j << std::endl;
    CGAL_assertion(i!=j);
    if(! _range_infos[i][j].is_computed) {
      compute_range_info(i,j);
    } else {
      //if(verbose) std::cout << "It is cached" << std::endl;
    }
    return _range_infos[i][j];
  }

  void compute_range_info(int i,int j) {
    //if(verbose) std::cout << "Compute_range_info: " << i << " " << j << std::endl;
    //if(verbose) std::cout << "Points: " << vertices_in_local_arr[i]->point().x() << " "<< vertices_in_local_arr[i]->point().y() << " and " << vertices_in_local_arr[j]->point().x() << " "<< vertices_in_local_arr[j]->point().y() << std::endl; 
    CGAL_assertion(i<j);
    Range_info& info = _range_infos[i][j];
    if(j==i+1) {
      info.is_triangulable=true;
      info.L_infty_bound=0.0;
      info.index_of_range=-1;
    } else {
      info.is_triangulable=false;
      std::vector<NT> L_infty_values;
      for(int k=i+1;k<j;k++) {
	
	NT L_infty_of_current=0.0;
	
	// Start checking the subinstances, if they are computed.
	// Otherwise, delay the computation to the end
	if(_range_infos[i][k].is_computed) {
	  Range_info& range1 = get_range_info(i,k);
	  //if(verbose) std::cout << "End Query range info for i=" << i << " and k=" << k << std::endl;
	  if(!range1.is_triangulable) {
	    //if(verbose) std::cout << "Is not triangulable" << std::endl;
	    continue;
	  }
	  //if(verbose) std::cout << "Is triangulable with L_infty " << range1.L_infty_bound << std::endl;
	  L_infty_of_current = std::max(L_infty_of_current,range1.L_infty_bound);
	  if(info.is_triangulable && info.L_infty_bound <= L_infty_of_current) {
	    continue; // since the triangulation will be worse
	  }	
	}
	if(_range_infos[k][j].is_computed) {
	  //if(verbose) std::cout << "Begin Query range info for k=" << k << " and j=" << j << std::endl;
	  Range_info& range2 = get_range_info(k,j);
	  //if(verbose) std::cout << "End Query range info for k=" << k << " and j=" << j << std::endl;
	  if(!range2.is_triangulable) {
	    //if(verbose) std::cout << "Is not triangulable" << std::endl;
	    continue;
	  }
	  //if(verbose) std::cout << "Is triangulable with L_infty " << range2.L_infty_bound << std::endl;
	  L_infty_of_current = std::max(L_infty_of_current,range2.L_infty_bound);
	  if(info.is_triangulable && info.L_infty_bound <= L_infty_of_current) {
	    continue; // since the triangulation will be worse
	  }
	}

	//if(verbose) std::cout << "Trying k=" << k << std::endl;
	//if(verbose) std::cout << "Point: " << vertices_in_local_arr[k]->point().x() << " "<< vertices_in_local_arr[k]->point().y() << std::endl;
	L_infty_values.clear();
	Edge_info& edge_ik = get_edge_info(i,k);

	if(!edge_ik.is_usable()) {
	  //if(verbose) std::cout << "Edge ik is not usable" << std::endl;
	  continue;
	}
	//if(verbose) std::cout << "Edge ik has L_infty value " << edge_ik.L_infty_dist << std::endl;
	L_infty_of_current = std::max(L_infty_of_current,edge_ik.L_infty_dist);
	if(info.is_triangulable && info.L_infty_bound <= L_infty_of_current) {
	  continue; // since the triangulation will be worse
	}
	Edge_info& edge_kj = get_edge_info(k,j);

	if(!edge_kj.is_usable()) {
	  //if(verbose) std::cout << "Edge kj is not usable" << std::endl;
	  continue;
	}
	//if(verbose) std::cout << "Edge kj has L_infty value " << edge_kj.L_infty_dist << std::endl;
	L_infty_of_current = std::max(L_infty_of_current,edge_kj.L_infty_dist);
	if(info.is_triangulable && info.L_infty_bound <= L_infty_of_current) {
	  continue; // since the triangulation will be worse
	}
	if(this->mind_L_infty_on_triangles) {
	  NT L_infty_triang = check_triangle(vertices_in_local_arr[i],
					     vertices_in_local_arr[j],
					     vertices_in_local_arr[k]);
	  //if(verbose) std::cout << "Triangle has L_infty value " << L_infty_triang << std::endl;
	  // Here potentially continue if too large elevation distance
	  L_infty_of_current = std::max(L_infty_of_current,L_infty_triang);
	  if(L_infty_threshold_set && L_infty_of_current > L_infty_threshold) {
	    continue;
	  }
	  if(info.is_triangulable && info.L_infty_bound <= L_infty_of_current) {
	    continue; // since the triangulation will be worse
	  }	
	}

	if(!_range_infos[i][k].is_computed) {
	  //if(verbose) std::cout << "Begin Query range info for i=" << i << " and k=" << k << std::endl;
	  Range_info& range1 = get_range_info(i,k);
	  //if(verbose) std::cout << "End Query range info for i=" << i << " and k=" << k << std::endl;
	  if(!range1.is_triangulable) {
	    //if(verbose) std::cout << "Is not triangulable" << std::endl;
	    continue;
	  }
	  //if(verbose) std::cout << "Is triangulable with L_infty " << range1.L_infty_bound << std::endl;
	  L_infty_of_current = std::max(L_infty_of_current,range1.L_infty_bound);
	  if(L_infty_threshold_set && L_infty_of_current > L_infty_threshold) {
	    continue;
	  }
	  if(info.is_triangulable && info.L_infty_bound <= L_infty_of_current) {
	    continue; // since the triangulation will be worse
	  }	
	}
	if(!_range_infos[k][j].is_computed) {
	  //if(verbose) std::cout << "Begin Query range info for k=" << k << " and j=" << j << std::endl;
	  Range_info& range2 = get_range_info(k,j);
	  //if(verbose) std::cout << "End Query range info for k=" << k << " and j=" << j << std::endl;
	  if(!range2.is_triangulable) {
	    //if(verbose) std::cout << "Is not triangulable" << std::endl;
	    continue;
	  }
	  //if(verbose) std::cout << "Is triangulable with L_infty " << range2.L_infty_bound << std::endl;
	  L_infty_of_current = std::max(L_infty_of_current,range2.L_infty_bound);
	  if(L_infty_threshold_set && L_infty_of_current > L_infty_threshold) {
	    continue;
	  }
	  if(info.is_triangulable && info.L_infty_bound <= L_infty_of_current) {
	    continue; // since the triangulation will be worse
	  }
	}
	
	//if(verbose) std::cout << "Found way of triangulating hole with L_infty " << L_infty_of_current << std::endl; 
	if(!info.is_triangulable || L_infty_of_current < info.L_infty_bound) {
	  //if(verbose) std::cout << "That is the new best way" << std::endl;
	  info.is_triangulable=true;
	  info.L_infty_bound=L_infty_of_current;
	  info.index_of_range=k;
#if 0
	  // Don't look for the best retriangulation, just any is fine
	  break;
#endif
	}
      }
    }
    info.is_computed=true;
    //if(verbose) std::cout << "Done with Compute_range_info: " << i << " " << j << std::endl;
    //if(verbose) std::cout << "Range Info: " << i << " " << j << " Is triangulable: " << info.is_triangulable << " L_infty bound: " << info.L_infty_bound << " Index_of_range: " << info.index_of_range << std::endl;
  }

  // This version avoids the use of CGAL::zone. However, it uses functions of Arrangement_2
  // that are no documented. An equivalent version with CGAL::zone is below
  // and currently commented out. It is slower by roughly a factor of 20
  bool compute_geometry_aware(Vertex_handle vp,Vertex_handle vq){
    //if(verbose) std::cout << "alt Geometry aware " << vp->point() <<  " " << vq->point() << std::endl;
    if(this->_is_strictly_convex) {
      return true;
    }
    Segment seg=construct_segment(vp->point(),vq->point());
    
    typedef CGAL::Arr_traits_adaptor_2<Arr_traits> Traits_adaptor;
    Traits_adaptor adaptor(*local_arr.traits());

    Traits_adaptor::Is_between_cw_2 between = adaptor.is_between_cw_2_object();
    
    Halfedge_handle first = vp->incident_halfedges();
    if(first->face()->is_unbounded()) {
      first=first->next()->twin();
    }
    CGAL_assertion(!first->face()->is_unbounded());
    CGAL_assertion(first->target()->point()==vp->point());
    Halfedge_handle second = first->next()->twin();
    CGAL_assertion(second->target()->point()==vp->point());
    
    bool eq1,eq2;

    bool ind = ! adaptor.equal_2_object()(adaptor.construct_max_vertex_2_object()(seg),vp->point());

    /*
    std::cout << "Seg: " << vp->point() << " - " << vq->point()  << std::endl;
    std::cout << "ind " << ind << std::endl;

    std::cout << "First: " << first->source()->point() << " - " << first->target()->point()  << std::endl;
    std::cout << "Direction: " << (first->direction()==CGAL::ARR_LEFT_TO_RIGHT) << std::endl;
    std::cout << "Second: " << second->source()->point() << " - " << second->target()->point()  << std::endl;
    std::cout << "Direction: " << (second->direction()==CGAL::ARR_LEFT_TO_RIGHT) << std::endl;

    std::cout << "Leffirst: " << first->curve().left() << std::endl;
    std::cout << "Leftsecond: " << second->curve().left() << std::endl;
    */

    if(! between(seg,ind,
		 first->curve(),(first->direction()==CGAL::ARR_RIGHT_TO_LEFT),
		 second->curve(),(second->direction()==CGAL::ARR_RIGHT_TO_LEFT),
		 vp->point(),
		 eq1,eq2) ) {
      return false;
    }
      
    Kernel::Do_intersect_2 intersect;
    Halfedge_handle start = vp->incident_halfedges(),run=start;
    do {
      //std::cout << "Source: " << run->source()->point() << " - " << run->target()->point() << std::endl;
      if(run->source()!=vp && run->target()!=vp && run->source()!=vq && run->target()!=vq) {
	if(intersect(Kernel::Segment_2(run->source()->point(),run->target()->point()),Kernel::Segment_2(vp->point(),vq->point()))) {
	  //std::cout << "Intersection with " << run->source()->point() << " - " << run->target()->point() << std::endl;
	  return false;
	}
      }
      run=run->prev();
    } while(run!=start);
    
    return true;
  }

  /*
  bool compute_geometry_aware(Vertex_handle vp,Vertex_handle vq){
    //if(verbose) std::cout << "Geometry aware " << vp->point() <<  " " << vq->point() << std::endl;
    if(this->_is_strictly_convex) {
      return true;
    }
    std::vector<CGAL::Object> objects;
    Segment seg=construct_segment(vp->point(),vq->point());
    
    CGAL::zone(local_arr,seg,std::back_inserter(objects));
    if(objects.size()>3) {
      return false;
    }
    
    CGAL_assertion_code(Vertex_handle end1);
    CGAL_assertion_code(Vertex_handle end2);
    CGAL_assertion(CGAL::assign(end1,objects[0]));
    CGAL_assertion(end1==vp||end1==vq);
    CGAL_assertion(CGAL::assign(end2,objects[2]));
    CGAL_assertion(end2==vp||end2==vq);
    CGAL_assertion(end1!=end2);
    
    Face_handle face;
    //if(verbose) std::cout << "Assign: " << CGAL::assign(face,objects[1]) << std::endl;
    //if(verbose) std::cout << "Is unbounded: " << face->is_unbounded() << std::endl;
    return CGAL::assign(face,objects[1]) && !face->is_unbounded();
  }
  */

  bool compute_persistent_aware(Vertex_handle vp,Vertex_handle vq) {
    CGAL_assertion(is_regular(curr_handle)); 
    //if(verbose) std::cout << "Compute persistence aware for " << vp->point() << ", " << vp->data().val << " and " << vq->point() << ", " << vq->data().val << " - Elevation at candidate: " << curr_handle->data().val << std::endl;
    if( (vp->data().val<=curr_handle->data().val && curr_handle->data().val<=vq->data().val) ||
	(curr_handle->data().val<=vp->data().val && vq->data().val<=curr_handle->data().val) ) {
      return true;
    }
    // So, either both vp and vq are above, or both are below
    bool vp_in_upper_link = curr_handle->data().val<vp->data().val;
    // Determine which direction to walk
    Halfedge_handle start=vp->incident_halfedges(), run=start;
    CGAL_assertion(start->target()==vp);
    while(run->target()!=vq && ( (vp_in_upper_link && curr_handle->data().val<=run->target()->data().val ) || (!vp_in_upper_link && run->target()->data().val<=curr_handle->data().val) )) {
      //if(verbose) std::cout << "In check: Vertex " << run->target()->point() << " with elev " << run->target()->data().val << std::endl;
      run=run->prev();
      //if(verbose) std::cout << "Next check: Vertex " << run->target()->point() << " with elev " << run->target()->data().val << std::endl;
    }
    if(run->target()!=vq) {
      start=start->next()->twin();
      CGAL_assertion(start->target()==vp);
    }
    CGAL_assertion_code(run=start;
			while(run->target()!=vq) {
			  //if(verbose) std::cout << "In assertion: Vertex " << run->target()->point() << " with elev " << run->target()->data().val << std::endl;
			  CGAL_assertion( (vp_in_upper_link && curr_handle->data().val <= run->target()->data().val ) || (!vp_in_upper_link && run->target()->data().val<=curr_handle->data().val) );
			  run=run->prev();
			}
			);
    run=start;
    run=run->prev();
    Vertex_handle threshold_vertex = vp_in_upper_link ? ( vp->data().val<vq->data().val ? vp : vq ) : (vq->data().val<vp->data().val ? vp : vq);
    while(run->target()!=vq) {
      Vertex_handle r = run->target();
      if(vp_in_upper_link) {
	if(r->data().val < threshold_vertex->data().val) {
	  return false;
	}
      } else {
	if(threshold_vertex->data().val < r->data().val) {
	  return false;
	}
      }
      run=run->prev();
    }
    return true;
  }    


  NT compute_L_infty_distance(Vertex_handle vp,Vertex_handle vq) {
    return compute_L_infty_distance_to(vp,vq,base_terrain);
    /*
    L_infty_timer.resume();
    //if(verbose) std::cout << "L_infty distance" << std::endl;
    //if(verbose) std::cout << "Elevation of " << vp->point().x() << " " << vp->point().y() << " : " << vp->data().val << std::endl;
    //if(verbose) std::cout << "Elevation of " << vq->point().x() << " " << vq->point().y() << " : " << vq->data().val << std::endl;
    std::vector<Point_triple> intersections;
    std::vector<Vertex_handle> vertices_encountered;
    CGAL::Object feature_at_vp,feature_at_vq;
    //get_inter_timer.resume();
    get_intersections_with_line_segment(base_terrain,
				       	vp,
					vq,
					intersections,
					vertices_encountered,
					feature_at_vp,
					feature_at_vq);
    //get_inter_timer.stop();
    // Only valid for generic input
    //CGAL_assertion(vertices_encountered.size()==0);
    //CGAL_assertion_code(Vertex_handle vh);
    //CGAL_assertion(CGAL::assign(vh,feature_at_vp));
    //CGAL_assertion(vh->point()==vp->point());
    //CGAL_assertion(CGAL::assign(vh,feature_at_vq));
    //CGAL_assertion(vh->point()==vq->point());
    
    //if(verbose) std::cout << "Found " << intersections.size() << " intersections" << std::endl;
    NT max_diff=0.0;
    
    // First check the elevation difference at the intersection points
    for(int i=0;i<intersections.size();i++) {
      Point r = intersections[i].first;
      Vertex_handle va = intersections[i].second;
      Vertex_handle vb = intersections[i].third;
      //Compute elevation in base terrain:
      NT elev_a=va->data().val;
      NT elev_b=vb->data().val;
      //if(verbose) std::cout << "Intersection Info:\n Intersection point: " << r << "\n base terrain a: " << va->point() << ", Elevation: " << elev_a
		<< "\n base terrain b: " << vb->point() << ", Elevation: " << elev_b << std::endl;

      NT elev_point_on_base_terrain = elevation_on_edge(r,va,vb);
      //if(verbose) std::cout << "Elevation in base terrain: " << elev_point_on_base_terrain << std::endl;
      NT elev_point_on_simplified_terrain= elevation_on_edge(r,vp,vq);
      //if(verbose) std::cout << "Elevation in simplified terrain: " << elev_point_on_simplified_terrain << std::endl;
      NT diff = CGAL::abs(elev_point_on_base_terrain-elev_point_on_simplified_terrain);
      //if(verbose) std::cout << "Difference: " << diff << std::endl;
      if(diff>max_diff) {
	max_diff=diff;
      }
    }
    // Now check the difference at encountered vertices of the base terrain
    for(int i=0;i<vertices_encountered.size();i++) {
      Vertex_handle vh = vertices_encountered[i];
      NT elev_point_on_base_terrain = vh->data().val;
      NT elev_point_on_simplified_terrain= elevation_on_edge(vh->point(),vp,vq);
      NT diff = CGAL::abs(elev_point_on_base_terrain-elev_point_on_simplified_terrain);
      //CGAL_assertion(CGAL::is_zero(diff));
      if(diff>max_diff) {
	max_diff=diff;
      }
    }
    
    // Check at vp 
    {
      NT elev_point_on_simplified_terrain = vp->data().val;
      NT elev_point_on_base_terrain=get_elevation_of_point_in_feature(vp->point(),feature_at_vp);
      //test_timer4.resume();
      NT diff = CGAL::abs(elev_point_on_base_terrain-elev_point_on_simplified_terrain);
      if(diff>max_diff) {
	max_diff=diff;
      }
      //test_timer4.stop();
    }
    // ..and at vq
    { 
      NT elev_point_on_simplified_terrain = vp->data().val;
      NT elev_point_on_base_terrain=get_elevation_of_point_in_feature(vq->point(),feature_at_vq);
      //test_timer4.resume();
      NT diff = CGAL::abs(elev_point_on_base_terrain-elev_point_on_simplified_terrain);
      if(diff>max_diff) {
	max_diff=diff;
      }
      //test_timer4.stop();
    }
    
    //if(verbose) std::cout << "max diff: " << max_diff << std::endl;
    L_infty_timer.stop();
    return max_diff;
    */
  }

  void compute_edge_info(int i,int j) {
    Edge_info& info = _edge_infos[i][j];
    int n = local_arr.number_of_vertices();
    if(i==j+1 || j==i+1 || (i==0 && j==n-1) || (i==n-1 && j==0)) { // adjacent vertices
      info.is_geometry_aware=true;
      info.is_persistent_aware=true;
      info.dist_too_large=false;
      // Alomg this edge, the L_infty distance is not always 0,
      // but if it is greater, it has been computed before,
      // so it seems unnecessary to re-compute it
      info.L_infty_dist=0.0;
    } else {
      Vertex_handle vp=vertices_in_local_arr[i];
      Vertex_handle vq=vertices_in_local_arr[j];
      info.is_geometry_aware=compute_geometry_aware(vp,vq);
      if(info.is_geometry_aware) {
	if(this->mind_persistent_aware) {
	  info.is_persistent_aware=compute_persistent_aware(vp,vq);
	  //if(verbose) std::cout << "Result of persistent aware : " << (info.is_persistent_aware ? "Yes" : "No") << std::endl;
	} else {
	  info.is_persistent_aware=true; // a hack...
	}
	
	if(info.is_persistent_aware) {
	  NT L_infty_distance(0);
	  if(this->mind_L_infty_on_edges) {
	    L_infty_distance = compute_L_infty_distance(vp,vq);
	  }
	  info.dist_too_large=(this->L_infty_threshold_set && this->L_infty_threshold < L_infty_distance);
	  info.L_infty_dist=L_infty_distance;
	}
      }
    }  
    info.is_computed=true;
    _edge_infos[j][i]=info;
  }


  NT check_triangle(Vertex_handle a, Vertex_handle b, Vertex_handle c) {
    //ch_tri_timer.resume();
    //if(verbose) std::cout << "Doing triangle " << a->point().x() << " " << a->point().y() << " - " << b->point().x() << " " << b->point().y() << " - " << c->point().x() << " " << c->point().y() << std::endl;
    //if(verbose) std::cout << "Elevations are " << a->data().val << " " << b->data().val << " " << c->data().val << std::endl;
    std::vector<Vertex_handle> vertices_in_triangle;

    triangular_range_search(base_terrain,
			    a,
			    b,
			    c,
			    vertices_in_triangle);
    //if(vertices_in_triangle.size()!=handles.size()) {
    //  std::cout << "CGAL returns " << handles.size() << " points, we found " << vertices_in_triangle.size() << std::endl;
    //}
    //CGAL_assertion(vertices_in_triangle.size()==handles.size());

    NT max_diff=0.0;

    for(int i=0;i<vertices_in_triangle.size();i++) {
      Vertex_handle vcurr = vertices_in_triangle[i];
      //if(verbose) std::cout << "Point " << vcurr->point() << std::endl;
      NT elev_base = vcurr->data().val;
      //if(verbose) std::cout << "Elevation in base terrain: " << elev_base << std::endl;
      //test_timer3.resume();
      NT elev_simplified = elevation_in_triangle(vcurr->point(),a,b,c);
      //test_timer3.stop();
	
      //if(verbose) std::cout << "Elevation in simplified terrain: " << elev_simplified << std::endl;
      NT diff = CGAL::abs(elev_base-elev_simplified);
      if(diff>max_diff) {
	max_diff=diff;
      }

    }

    //if(verbose) std::cout << "Maximal diff is " << max_diff << std::endl;
    //ch_tri_timer.stop();
    return max_diff;
  }

public:

  Edge_info& get_edge_info(int i,int j) {
    CGAL_assertion(i!=j);
    if(! _edge_infos[i][j].is_computed) {
      compute_edge_info(i,j);
    }
    return _edge_infos[i][j];
  }


  Link_retriangulator(Arrangement& base_terrain, 
		      bool mind_persistent_aware=true,
		      bool keep_critical_points=true)
    : base_terrain(base_terrain),mind_persistent_aware(mind_persistent_aware), keep_critical_points(keep_critical_points), mind_L_infty_on_edges(true), mind_L_infty_on_triangles(true),L_infty_threshold_set(false)
  {
  }

  void set_flags(bool mind_persistent_aware,
		 bool keep_critical_points,
		 bool mind_L_infty_on_edges,
		 bool mind_L_infty_on_triangles) {
    this->mind_persistent_aware=mind_persistent_aware;
    this->keep_critical_points=keep_critical_points;
    this->mind_L_infty_on_edges=mind_L_infty_on_edges;
    this->mind_L_infty_on_triangles=mind_L_infty_on_triangles;
  }

  void set_L_infty_threshold(NT threshold) {
    this->L_infty_threshold_set=true;
    this->L_infty_threshold=threshold;
  }

  void check_strict_convexity() {
    Face_handle face=local_arr.faces_begin();
    if(face->is_unbounded()) {
      face++;
    }
    Kernel::Orientation_2 orient;
    
    Arrangement::Ccb_halfedge_circulator edges=face->outer_ccb(),
      start=edges;
    Point p = start->source()->point();
    Point q = start->target()->point();
    this->_is_strictly_convex=true;
    do {
      edges++;
      Point r = edges->target()->point();
      //if(verbose) std::cout << "Orient: " << p.x() << " " << p.y() << " - " << q.x() << " " << q.y() << " - " << r.x() << " " << r.y() << std::endl;
      if(_is_strictly_convex && orient(p,q,r)!=CGAL::LEFT_TURN) {
	_is_strictly_convex=false;
	break;
      }
      p=edges->source()->point();
      q=r;
    } while(edges!=start);
  }

  void collect_edges(int i,int j,std::vector<Vertex_pair>& edges) {
    CGAL_assertion(_range_infos[i][j].is_computed);
    int k=get_range_info(i,j).index_of_range;
    if(k==-1) {
      return;
    }
    if(k!=i+1) {
      //if(verbose) std::cout << " Collecting " << vertices_in_base_arr[i]->point() << " - " << vertices_in_base_arr[k]->point() << std::endl;
      edges.push_back(std::make_pair(vertices_in_base_arr[i],
				     vertices_in_base_arr[k]));
      //edges.push_back(construct_segment(vertices_in_local_arr[i]->point(),vertices_in_local_arr[k]->point()));
    }
    if(k+1!=j) {
      //if(verbose) std::cout << " Collecting " << vertices_in_base_arr[k]->point() << " - " << vertices_in_base_arr[j]->point() << std::endl;
      edges.push_back(std::make_pair(vertices_in_base_arr[k],
				     vertices_in_base_arr[j]));
		      //edges.push_back(construct_segment(vertices_in_local_arr[k]->point(),vertices_in_local_arr[j]->point()));
    }
    collect_edges(i,k,edges);
    collect_edges(k,j,edges);
  }
    
		    
  // 1st argument denotes whether the link of the vertex can be retriangulated
  // 2nd argument gives a bound on the L_inft distance for the triangulation
  // edges_of_retriangulation gives the edges that triangulate the link
  std::pair<bool,NT> operator()(Vertex_handle v,std::vector<Vertex_pair>& edges_of_retriangulation) {
    
    //if(verbose) std::cout << "YY: Now we check " << v->point().x() << " " << v->point().y() << std::endl;

    if(is_at_boundary(v)) {
      //if(verbose) std::cout << "is at boundary, not eligible" << std::endl;
      return std::make_pair(false,0.0);
    }

    if(this->keep_critical_points && !is_regular(v)) {
      //if(verbose) std::cout << "Not regular, not eligible" << std::endl;
      return std::make_pair(false,0.0);
    }

    edges_of_retriangulation.clear();
    local_arr.clear();
    vertices_in_local_arr.clear();
    vertices_in_base_arr.clear();
    this->curr_handle=v;
    // Build up local_arr
    //if(verbose) std::cout << "Build up local arr" << std::endl;
    Halfedge_circulator circ = v->incident_halfedges(), start=circ;
    do {
      Halfedge_handle next_edge=circ->prev();
      Vertex_handle vh=next_edge->target();
      CGAL_assertion(vh!=v);
      Vertex_handle local_vh=CGAL::insert_point(local_arr,vh->point());
      local_vh->set_data(vh->data());
      vertices_in_local_arr.push_back(local_vh);
      vertices_in_base_arr.push_back(vh);
      CGAL::insert_non_intersecting_curve(local_arr,next_edge->curve());
      circ++;
    } while(circ!=start);
    //if(verbose) std::cout << "Done, " << local_arr.number_of_vertices() << " vertices and " << local_arr.number_of_halfedges() << " halfedges" << std::endl;
    CGAL_assertion(local_arr.number_of_vertices()==v->degree());
    CGAL_assertion(local_arr.number_of_halfedges()==2*v->degree());
    CGAL_assertion(local_arr.number_of_faces()==2);
    
    check_strict_convexity();
    //if(verbose) std::cout << "Is strictly convex: " << (_is_strictly_convex ? "Yes" : "No") << std::endl;
    
    int n = vertices_in_local_arr.size();
    CGAL_assertion(n==v->degree());
    _edge_infos.resize(n);
    _range_infos.resize(n);
    for(int i=0;i<n;i++) {
      _edge_infos[i].resize(n);
      _range_infos[i].resize(n);
      
      for(int j=0;j<n;j++) {
	_edge_infos[i][j]=Edge_info();
	_range_infos[i][j]=Range_info();
      }
    }
    Range_info& info = get_range_info(0,n-1);
    if(info.is_triangulable) {
      collect_edges(0,n-1,edges_of_retriangulation);
      //if(verbose) std::cout << "Fetched " << edges_of_retriangulation.size() << " edges for re-triangulation" << std::endl;
      CGAL_assertion(edges_of_retriangulation.size()==n-3);
    }
    return std::make_pair(info.is_triangulable,info.L_infty_bound);
  }
    
  
};


template<typename OutputIterator>
void read_points(std::istream& instr,OutputIterator out) {
  while(instr.good()) {
    double x,y,v;
    instr >> x >> y >> v;
    *out++ = std::make_pair(Point(x,y),v);
  }
}

bool is_triangle(Face_handle f) {
  if(f->is_unbounded()) {
    return false;
  }
  Arrangement::Ccb_halfedge_circulator circ=f->outer_ccb(), start=circ;
  int size_of_outer_ccb=1;
  circ++;
  while(circ!=start) {
    size_of_outer_ccb++;
    circ++;
  }
  if(size_of_outer_ccb!=3) {
    return false;
  }
  if(f->holes_begin()!=f->holes_end()) {
    return false;
  }
  return true;
}



bool is_triangulation(Arrangement& arr) {
  if(arr.number_of_isolated_vertices()!=0) {
    //if(verbose) std::cout << "Arrangememnt has isolated vertices!";
    return false;
  }
  if(arr.number_of_unbounded_faces()!=1) {
    //if(verbose) std::cout << "The arrangement has " << arr.number_of_unbounded_faces() << "unbounded faces, something seems wrong!" << std::endl;
    return false;
  }
  
  for(Arrangement::Face_iterator f = arr.faces_begin();
      f!=arr.faces_end();
      f++) {
    if(f->is_unbounded()) {
      continue;
    }
    Arrangement::Ccb_halfedge_circulator circ=f->outer_ccb(), start=circ;
    int size_of_outer_ccb=1;
    circ++;
    while(circ!=start) {
      size_of_outer_ccb++;
      circ++;
    }
    if(size_of_outer_ccb!=3) {
      //if(verbose) std::cout << "Face with " << size_of_outer_ccb << " found" << std::endl;
      return false;
    }
    if(f->holes_begin()!=f->holes_end()) {
      //if(verbose) std::cout << "Face with holes found" << std::endl;
      return false;
    }
  }
  int euler = arr.number_of_vertices()-arr.number_of_edges()+arr.number_of_faces();
  if(euler!=2) {
    //if(verbose) std::cout << "Euler Characteristic is " << euler << std::endl;
    return false;
  }
  return true;
}

struct Comp_segments_by_length {
  
  bool operator() (const Segment& s, const Segment& t) {
    Kernel::Compare_distance_2 comp;
    CGAL::Comparison_result res = comp(s.source(),s.target(),
				       t.source(),t.target());
    return res == CGAL::SMALLER;
  }

};

double seg_length(const Segment& s) {
  return sqrt(CGAL::to_double(Kernel::Compute_squared_distance_2()(s.source(),s.target())));
}


template<typename InputIterator>
void create_arrangement(Arrangement& arr,
			Ric_point_location& point_location,
			bool filter_long_edges,
			double filter_threshold,
			InputIterator begin,
			InputIterator end) {
  std::vector<Point> points;
  for(InputIterator it=begin;it!=end;it++) {
    points.push_back(it->first);
  }
  std::cout << "Compute Delaunay triangulation" << std::endl;
  Delaunay_triangulation del;
  del.insert(points.begin(),points.end());
  std::vector<Segment> segments;
  for(Delaunay_triangulation::Finite_edges_iterator edge_it=del.finite_edges_begin();
      edge_it!=del.finite_edges_end();
      edge_it++) {
    Kernel::Segment_2 seg=del.segment(*edge_it);
    //if(verbose) std::cout << seg.source().x() << ", " << seg.source().y() << " -- " << seg.target().x() << ", " << seg.target().y() << std::endl;
    segments.push_back(Segment(seg));    
  }
  

  if(filter_long_edges) {

    std::cout << "Filter edges, start with " << segments.size() << " Segments" << std::endl;
    std::sort(segments.begin(),segments.end(),Comp_segments_by_length());
    int n = segments.size();
    Segment& median = segments[n/2];
    NT median_length = seg_length(median);
    std::cout << "Length of median: " << median_length << std::endl;
    for(int i=n-1;i>0;i--) {
      if(seg_length(segments[i]) > filter_threshold*median_length) {
	segments.pop_back();
      }
    }
    std::cout << "Filtering done, " << segments.size() << " segments remaining" << std::endl;
  }

  std::cout << "Creating the base terrain" << std::endl;
  // Now add the segments
  CGAL::insert_non_intersecting_curves(arr,segments.begin(),segments.end());
  point_location.attach(arr);
  CGAL_assertion_code(int n = arr.number_of_vertices());
  // Assign data to vertices
  std::cout << "Assign elevation data" << std::endl;
  for(InputIterator it=begin;it!=end;it++) {
    Vertex_handle v = CGAL::insert_point(arr,it->first,point_location);
    v->set_data(Data(it->second));
  }
  set_lower_star_data(arr);
  set_hints(arr);
  CGAL_assertion(n==arr.number_of_vertices());
  if(! is_triangulation(arr)) {
    std::cout << "Is not a triangulationof a simply-connected domain" << std::endl;
    std::exit(1);
  }
  std::cout << "Done creating the base terrain" << std::endl;
}


    



template<typename OutputIterator>
void remove_vertex_with_star(Arrangement& arr, Vertex_handle v, OutputIterator neighbors_of_v) {
  //if(verbose) std::cout << "Info: Vertex has degree " << v->degree() << std::endl;
  std::vector<Halfedge_handle> incident_halfedges;
  Arrangement::Halfedge_around_vertex_circulator circ=v->incident_halfedges(),
    start=circ;
  // First collect the segments for later usage, and the halfedges
  do {
    incident_halfedges.push_back(circ);
    CGAL_assertion(circ->target()==v);
    neighbors_of_v++=circ->source();
    circ++;
  } while(circ!=start);
  // Now remove
  for(int i=0;i<incident_halfedges.size();i++) {
    //if(verbose) std::cout << "Removing segment " << incident_halfedges[i]->source()->point() << " - " << incident_halfedges[i]->target()->point() << std::endl;
    // do not remove the vertex
    arr.remove_edge(incident_halfedges[i],false,false);
  }
  CGAL_assertion(v->is_isolated());
  arr.remove_isolated_vertex(v);
}

template<typename InputIterator,typename OutputIterator>
void remove_and_retriangulate(Arrangement& arr, 
			      Vertex_handle v,
			      InputIterator segments_begin,
			      InputIterator segments_end,
			      OutputIterator neighbors_of_v) {
  // Remove vertex and incident edges
  remove_vertex_with_star(arr,v,neighbors_of_v);
  
  for(InputIterator it = segments_begin;it!=segments_end;it++) {
    //if(verbose) std::cout << it->first->point() << " - " << it->second->point() << std::endl;
    Segment s(it->first->point(),it->second->point());
    //if(verbose) std::cout << "Segment " << ": " << s << std::endl;
    arr.insert_at_vertices(s,it->first,it->second);
    //CGAL::insert_non_intersecting_curve(arr,s);
  }
  CGAL_assertion(is_triangulation(arr));
}

class Greedy_simplifier {

protected:
  Arrangement& terrain;
  Link_retriangulator& retriangulator;
  std::set<Vertex_handle> vertex_to_check;
  std::vector<Vertex_handle> mirrored_vertex_set;
  int number_of_attempts;

public:

  Greedy_simplifier(Arrangement& terrain, Link_retriangulator& retriangulator) 
    : terrain(terrain), retriangulator(retriangulator), number_of_attempts(0) 
  {
    srand(time(NULL));
    for(Arrangement::Vertex_iterator vit = terrain.vertices_begin();
	vit!=terrain.vertices_end();
	vit++) {
      vertex_to_check.insert(vit);
      mirrored_vertex_set.push_back(vit);
      CGAL_assertion(vertex_to_check.size()==mirrored_vertex_set.size());
    }
  }

  void reset_number_of_attempts() {
    this->number_of_attempts=0;
  }

  int get_number_of_attempts() {
    return this->number_of_attempts;
  }

  bool remove_next_vertex() {
    bool element_found=false;

    while(!vertex_to_check.empty() && !element_found) {
      
      CGAL_assertion(vertex_to_check.size()==mirrored_vertex_set.size());
      int r = rand()%mirrored_vertex_set.size();
      //std::cout << "Random choice: " << r << " of " << mirrored_vertex_set.size() << std::endl;
      Vertex_handle& next = mirrored_vertex_set[r];
      
      //std::cout << "Trying " << next->point() << std::endl;
      
      std::vector<Vertex_pair> edges;
      std::pair<bool,NT> result = retriangulator(next,edges);
      number_of_attempts++;

      if(result.first) {
	//std::cout << "Success!" << std::endl;
	element_found=true;
	std::vector<Vertex_handle> neighbors;
	remove_and_retriangulate(terrain,
				 next,
				 edges.begin(),edges.end(),
				 std::back_inserter(neighbors));
	for(int i=0;i<neighbors.size();i++) {
	  if(vertex_to_check.find(neighbors[i])==vertex_to_check.end()) {
	    //std::cout << "Add neighbor " << neighbors[i]->point() << std::endl;
	    vertex_to_check.insert(neighbors[i]);
	    mirrored_vertex_set.push_back(neighbors[i]);
	  }
	}
      }
      int elements_removed = vertex_to_check.erase(next);
      CGAL_assertion(elements_removed==1);
      mirrored_vertex_set[r]=mirrored_vertex_set.back();
      mirrored_vertex_set.pop_back();
    }

    //std::cout << "Number of attempts: " << number_of_attempts << std::endl;
    return element_found;
  }


};

class Heap_simplifier {
  
  struct Retriangulation_info {
    Vertex_handle v;
    NT L_infty_dist;
    std::vector<Vertex_pair> edges;
    Retriangulation_info(Vertex_handle v,NT d, std::vector<Vertex_pair>& segs) 
      : v(v),L_infty_dist(d), edges(segs)
    {}

    Retriangulation_info(Vertex_handle v) : v(v) {}

  };

  class Compare_info_pointers {
  public:
    bool operator() (Retriangulation_info* a, Retriangulation_info* b) {
      return (a->L_infty_dist > b->L_infty_dist);
    }
  };

protected:
  Arrangement& terrain;
  Link_retriangulator& retriangulator;
  std::vector<Retriangulation_info*> removal_heap;
  std::unordered_map<Vertex_handle,Retriangulation_info*> vertex_to_info_map;
  Compare_info_pointers compare;

public:
  long count_retriang=0;

public:
  Heap_simplifier(Arrangement& terrain, Link_retriangulator& retriangulator) 
    : terrain(terrain), retriangulator(retriangulator), count_retriang(0)
  {}

  void initialize_heap() {
    test_timer1.resume();
    removal_heap.clear();
    for(Arrangement::Vertex_iterator v_it = terrain.vertices_begin();
	v_it!=terrain.vertices_end();
	v_it++) {
      //if(verbose) std::cout << "Vertex: " << v_it->point().x() << ", " << v_it->point().y() << " Elevation:" << v_it->data().val << " Degree: " << v_it->degree() << " On bd: " << is_at_boundary(v_it) << std::endl;
      Point p = v_it->point();
      std::vector<Vertex_pair> edges;
      std::pair<bool,NT> result = retriangulator(v_it,edges);
      
      if(result.first) { //Re-triangulation is possible
	Retriangulation_info* new_info 
	  = new Retriangulation_info(v_it,result.second,edges);
	removal_heap.push_back(new_info);
	vertex_to_info_map[v_it]=new_info;
      }
    }
    //if(verbose) std::cout << "Heapify now " << removal_heap.size() << " elements" << std::endl;
    std::make_heap(removal_heap.begin(),removal_heap.end(),compare);
    test_timer1.stop();
  
  }

  void print_heap(int elements_to_print=std::numeric_limits<int>::max()) {
    std::cout << "XX Printing heap: " << removal_heap.size() << " elements (Terrain has " << terrain.number_of_vertices() << " vertices)" << std::endl;
    std::vector<Retriangulation_info*> backup;
    int count=0;
    int elements_printed=0;
    while(removal_heap.size()>0) {
      std::pop_heap(removal_heap.begin(),removal_heap.end(),compare);
      Retriangulation_info* next = removal_heap.back();
      removal_heap.pop_back();
      if(elements_printed<=elements_to_print) {
	std::cout << "Next heap element " << count++ << ": " << next->v->point().x() << " " << next->v->point().y() << " -> L_infty=" << next->L_infty_dist << " No segments stored: " << next->edges.size() << " (Degree of vertex is " << next->v->degree() << " )" << std::endl;
	elements_printed++;
      }
      backup.push_back(next);
      CGAL_assertion(next->v->degree()==next->edges.size()+3);
    }
    std::cout << "Done, push elements back to heap" << std::endl;
    std::copy(backup.begin(),backup.end(),std::back_inserter(removal_heap));
    std::make_heap(removal_heap.begin(),removal_heap.end(),compare);
  }

  bool empty() {
    return removal_heap.size()==0;
  }

  NT peek_min_value() {
    return removal_heap.front()->L_infty_dist;
  }

  NT remove_next_vertex (){
    
    std::pop_heap(removal_heap.begin(),removal_heap.end(),compare);
    Retriangulation_info* next = removal_heap.back();
    NT result = next->L_infty_dist;
    removal_heap.pop_back();
    vertex_to_info_map.erase(next->v);
    //if(verbose) std::cout << "Now removing element: " << next->v->point().x() << " " << next->v->point().y() << " -> L_infty=" << next->L_infty_dist << " No segments stored: " << next->edges.size() << " (Degree of vertex is " << next->v->degree() << " )" << std::endl;
    std::vector<Vertex_handle> neighbors;
    remove_and_retriangulate(terrain,next->v,next->edges.begin(),next->edges.end(),std::back_inserter(neighbors));
    // Update neighbors
    for(int i=0;i<neighbors.size();i++) {
      Vertex_handle w = neighbors[i];

      //if(verbose) std::cout << "XX Re-analyzing neighbor: " << w->point().x() << " " << w->point().y() << std::endl;

      bool vertex_exists_in_heap 
	= vertex_to_info_map.find(w)!=vertex_to_info_map.end();

      ////if(verbose) std::cout << "Exists in heap: " << (vertex_exists_in_heap ? "Yes" : "No") << std::endl;

      std::vector<Vertex_pair> edges;
      test_timer2.resume();
      count_retriang++;
      std::pair<bool,NT> result = retriangulator(w,edges);
      test_timer2.stop();
      //if(verbose) std::cout << "XX Re-analyzed neighbor: " << w->point().x() << " " << w->point().y() << std::endl;
     
      if(result.first) {
	Retriangulation_info* info;
	if(vertex_exists_in_heap) {
	  //if(verbose) std::cout << "XX: Update Retriangulation info" << std::endl;
	  info = vertex_to_info_map[w];
	} else {
	  //if(verbose) std::cout << "XX: New Retriangulation element" << std::endl;
	  info = new Retriangulation_info(w);
	  removal_heap.push_back(info);
	  vertex_to_info_map[w]=info;
	}
	info->L_infty_dist=result.second;
	info->edges=edges;
	CGAL_assertion(info->v->degree()==info->edges.size()+3);
      } else {
	if(vertex_exists_in_heap) {
	  //if(verbose) std::cout << "XX: Delete Retriangulation info" << std::endl;
	  Retriangulation_info* info = vertex_to_info_map[w];
	  for(int i=0;i<removal_heap.size();i++) {
	    if(removal_heap[i]==info) {
	      removal_heap[i]=removal_heap.back();
	      break;
	    }
	  }
	  removal_heap.pop_back();
	  delete info;
	  vertex_to_info_map.erase(w);
	} else {
	  //if(verbose) std::cout << "XX: Do nothing" << std::endl;
	  // Do nothing
	}
      }
      
      
    }
    std::make_heap(removal_heap.begin(),removal_heap.end(),compare);
    //if(verbose) std::cout << "Heapified again, size of removal heap: " << removal_heap.size() << " and of vertex_to_info_map: " << vertex_to_info_map.size() << std::endl;
    CGAL_assertion(vertex_to_info_map.size()==removal_heap.size());
    delete next;
    
    return result;
  }
};

// For convenience: Check that no triple is collinear 
// and no two elevations are equal
bool check_genericity(std::vector<std::pair<Point,NT> >& points) {
  int n = points.size();
  Kernel::Orientation_2 orient;
  for(int i=0;i<n;i++) {
    for(int j=i+1;j<n;j++) {
      if(points[i].second==points[j].second) {
	/*
	std::cout << "Detected points with same elevation:\n"
		  << points[i].first.x() << " " << points[i].first.y() << " Elev: " << points[i].second << "\n"
		  << points[j].first.x() << " " << points[j].first.y() << " Elev: " << points[j].second << std::endl;
	return false;
	*/
      }
      for(int k=j+1;k<n;k++) {
	if(orient(points[i].first,points[j].first,points[k].first)==CGAL::COLLINEAR) {
	  std::cout << "Detected collinear triple: \n"
		    << points[i].first.x() << " " << points[i].first.y() << "\n"
		    << points[j].first.x() << " " << points[j].first.y() << "\n"
		    << points[k].first.x() << " " << points[k].first.y() << std::endl;
	  return false;
	}
	
      }
    }
  }
  // never reached
  return true;
}

std::string suffix_for_simplified_terrain(Arrangement& terrain, NT delta, std::string suffix) {
  int n = terrain.number_of_vertices();
  std::stringstream sstr;
  sstr << "simplified_" << n <<  "_delta_" << CGAL::to_double(delta);
  if(suffix!="") {
    sstr << "_" << suffix;
  }
  return sstr.str();
}

void write_to_obj_file(Arrangement& terrain, std::string input_filename_pre,std::string suffix="") {
  std::string input_filename = input_filename_pre+"_"+suffix+".obj";
  std::cout << "Creating file " << input_filename << std::endl;
  std::ofstream out(input_filename.c_str());
  std::unordered_map<Vertex_handle,int> vertex_index;
  int index=1;
  for(Arrangement::Vertex_iterator vit = terrain.vertices_begin();
      vit!=terrain.vertices_end();
      vit++) {
    out << "v " << vit->point().x() << " " << vit->point().y() << " " << vit->data().val << std::endl;
    vertex_index[vit]=index++;
  }
  for(Arrangement::Face_iterator fit = terrain.faces_begin();
      fit!=terrain.faces_end();
      fit++) {
    if(fit->is_unbounded()) {
      continue;
    }
    out << "f ";
    Arrangement::Ccb_halfedge_circulator start=fit->outer_ccb(), run=start;
    out << vertex_index[run->source()] << " ";
    run++;
    out << vertex_index[run->source()] << " ";
    run++;
    out << vertex_index[run->source()] << " ";
    run++;
    CGAL_assertion(run==start);
    out << std::endl;
  }
  for(Arrangement::Halfedge_iterator eit = terrain.halfedges_begin();
      eit!=terrain.halfedges_end();
      eit++) {
    out << "l " << vertex_index[eit->source()] << " " << vertex_index[eit->target()] << std::endl;
  }
  out.close();
}
  
// What follows is just used for testing purposes
template<typename PointLocation>
NT elevation_at_point(Arrangement& arr, Point p, PointLocation& pl) {
  CGAL::Arr_point_location_result<Arrangement>::Type obj = pl.locate(p);
  typedef Arrangement::Vertex_const_handle   Vertex_const_handle;
  typedef Arrangement::Halfedge_const_handle Halfedge_const_handle;
  typedef Arrangement::Face_const_handle     Face_const_handle;
  const Vertex_const_handle*   v;
  const Halfedge_const_handle* e;
  const Face_const_handle*     f;
  if ( ( f = boost::get<Face_const_handle>(&obj) ) ) {
    //if(verbose) std::cout << "Face" << std::endl;
    Face_handle fh = arr.non_const_handle(*f);
    Arrangement::Ccb_halfedge_circulator run=fh->outer_ccb();
    Vertex_handle a =run->source();
    run++;
    Vertex_handle b =run->source();
    run++;
    Vertex_handle c =run->source();
    run++;
    //if(verbose) std::cout << a->point() << ": " << a->data().val << " -- "  << b->point() << ": " << b->data().val << " -- "  << c->point() << ": " << c->data().val << std::endl;
    return elevation_in_triangle(p,a,b,c);
  } else if ( ( e = boost::get<Halfedge_const_handle>(&obj) ) ) {
    //if(verbose) std::cout << "Edge" << std::endl;
    Halfedge_handle eh = arr.non_const_handle(*e);
    //if(verbose) std::cout << eh->source()->point() << " elev: " << eh->source()->data().val << " and " << eh->target()->point() << " elev: " << eh->target()->data().val << std::endl;
    return elevation_on_edge(p,eh->source(),eh->target());
  } else if ( ( v = boost::get<Vertex_const_handle>(&obj) ) ) {
    //if(verbose) std::cout << "Vertex" << std::endl;
    return (*v)->data().val;
  } else {
    CGAL_error_msg("Invalid object");
  }
  return 0.0;
  
}

// Only used for testing correctness of the output
template<typename PointLocation1, typename PointLocation2>
NT L_infty_distance_terrains(Arrangement& arr1, Arrangement& arr2,
			     PointLocation1& point_location_1,
			     PointLocation2& point_location_2) {
  CGAL::Arr_default_overlay_traits<Arrangement> overlay_traits;
  Arrangement arr_overlay;
  CGAL::overlay(arr1,arr2,arr_overlay,overlay_traits);
  NT max_diff=0.0;
  for(Arrangement::Vertex_iterator vit = arr_overlay.vertices_begin();
      vit!=arr_overlay.vertices_end();
      vit++) {
    //if(verbose) std::cout << "Found vertex " << vit->point() << std::endl;
    NT elev1 = elevation_at_point(arr1,vit->point(),point_location_1);
    NT elev2 = elevation_at_point(arr2,vit->point(),point_location_2);
    NT diff = CGAL::abs(elev1-elev2);
    //if(verbose) std::cout << "Difference is |" << elev1 << "-" << elev2 << "|=" << diff << std::endl;
    if(diff>max_diff) {
      max_diff=diff;
    }
  }
  return max_diff;
}

////////////////////////////////////////////////////////////////////////////////

typedef std::pair<NT,NT> Persistence_point;
typedef std::vector<Persistence_point> Persistence_diagram;

struct Compare_by_data {

  bool operator() (Halfedge_handle a , Halfedge_handle b) {
    if(a->data().val!=b->data().val) {
      return a->data().val < b->data().val;
    }
    return a < b;
  }
};



void compute_persistence(Arrangement& terrain, 
			 Persistence_diagram& dgm_0,
			 Persistence_diagram& dgm_1) {
  Compare_by_data compare;
  dgm_0.clear();
  dgm_1.clear();
  
  CGAL::Union_find<NT> uf_0,uf_1;
  typedef CGAL::Union_find<NT>::handle UFH;

  std::unordered_map<Vertex_handle,UFH> vertex_to_handle;
  std::unordered_map<Face_handle,UFH> face_to_handle;

  // Set up union find sets
  for(Arrangement::Vertex_iterator vit = terrain.vertices_begin();
      vit!=terrain.vertices_end();
      vit++) {
    UFH ufh=uf_0.make_set(vit->data().val);
    vertex_to_handle[vit]=ufh;
  }

  // Compute edge filtration
  std::vector<Halfedge_handle> edges;
  for(Arrangement::Edge_iterator eit = terrain.edges_begin();
      eit!=terrain.edges_end();
      eit++) {
    edges.push_back(eit);
  }
  std::sort(edges.begin(),edges.end(),compare);

  for(int i=0;i<edges.size();i++) {
    //if(verbose) std::cout << "Value: " << edges[i]->data().val << ": " << edges[i]->source()->point() << " - " << edges[i]->target()->point()<< std::endl;
    Vertex_handle v=edges[i]->source();
    Vertex_handle w=edges[i]->target();
    UFH hv=uf_0.find(vertex_to_handle[v]);
    UFH hw=uf_0.find(vertex_to_handle[w]);
    if(hv==hw) {
      continue;
    }
    NT birth = std::max(*hv,*hw);
    NT death = edges[i]->data().val;
    //if(verbose) std::cout << "BD: " << birth << " " << death << std::endl;
    CGAL_assertion(birth<=death);
    if(birth<death) {
      //if(verbose) std::cout << "New persistence pair (dim 0): " << birth << " " << death << std::endl;
      dgm_0.push_back(std::make_pair(birth,death));
    } else {
      //if(verbose) std::cout << "Pair of persistence 0 created, ignored" << std::endl;
    }
    uf_0.unify_sets(hv,hw);
    UFH hnew = uf_0.find(hv);
    *hnew = std::min(*hv,*hw);
  }


  //if(verbose) std::cout << "Dim 1" << std::endl;
  // Set up union sets in dim 1 using triangles
  for(Arrangement::Face_iterator fit = terrain.faces_begin();
      fit!=terrain.faces_end();
      fit++) {
    UFH ufh=uf_1.make_set(fit->data().val);
    face_to_handle[fit]=ufh;
  }
  
  // Compute persistence in dim 1 
  for(int i=edges.size()-1;i>=0;i--) {
    //if(verbose) std::cout << "Value: " << edges[i]->data().val << ": " << edges[i]->source()->point() << " - " << edges[i]->target()->point()<< std::endl;
    Face_handle f = edges[i]->face();
    Face_handle g = edges[i]->twin()->face();
    
    UFH hf=uf_1.find(face_to_handle[f]);
    UFH hg=uf_1.find(face_to_handle[g]);
    if(hf==hg) {
      continue;
    }

    NT death = std::min(*hf,*hg);
    NT birth = edges[i]->data().val;
    CGAL_assertion(birth<=death);
    if(birth<death) {
      //if(verbose) std::cout << "New persistence pair (dim 1): " << birth << " " << death << std::endl;
      dgm_1.push_back(std::make_pair(birth,death));
    } else {
      //if(verbose) std::cout << "Pair of persistence 0 created, ignored" << std::endl;
    }
    uf_1.unify_sets(hf,hg);
    UFH hnew = uf_1.find(hf);
    *hnew = std::max(*hf,*hg);
  }   

}
			 
void print_persistence_diagram(Persistence_diagram& dgm, std::string prefix) {
  std::cout << prefix << std::endl;
  for(int i=0;i<dgm.size();i++) {
    std::cout << dgm[i].first << " " << dgm[i].second << std::endl;
  }

}

////////////////////////////////////////////////////////////////////////////////

struct Graph_vertex {
  Vertex_handle v;
  Halfedge_handle e;
  Face_handle f;
  int dim;
  bool visited;
  Graph_vertex() : visited(false) {}
  Graph_vertex(Vertex_handle v) : v(v), dim(0), visited(false) {}
  Graph_vertex(Halfedge_handle e) : e(e), dim(1), visited(false) {}
  Graph_vertex(Face_handle f) : f(f), dim(2), visited(false) {}
};

struct UGraph_edge {
  Halfedge_handle e;
  UGraph_edge() {}
  UGraph_edge(Halfedge_handle e) :e(e) {}
};

typedef boost::adjacency_list<boost::setS,boost::setS,boost::undirectedS,Graph_vertex,UGraph_edge> UGraph;
typedef boost::adjacency_list<boost::setS,boost::setS,boost::bidirectionalS,Graph_vertex> DiGraph;
typedef boost::graph_traits<UGraph>::vertex_descriptor UGraph_vertex_t;
typedef boost::graph_traits<DiGraph>::vertex_descriptor DiGraph_vertex_t;


void simplification_low_pers_pairs(Arrangement& terrain,
				   NT delta,
				   UGraph& M_delta,
				   std::set<UGraph_vertex_t>& R) {
  Compare_by_data compare;

  CGAL::Union_find<Vertex_handle> uf_0;
  CGAL::Union_find<Face_handle>uf_1;
  typedef CGAL::Union_find<Vertex_handle>::handle UFHv;
  typedef CGAL::Union_find<Face_handle>::handle UFHf;

  std::unordered_map<Vertex_handle,UFHv> vertex_to_handle;
  std::unordered_map<Face_handle,UFHf> face_to_handle;

  std::unordered_map<Vertex_handle,UGraph_vertex_t> vertex_to_graph;
  std::unordered_map<Face_handle,UGraph_vertex_t> face_to_graph;

  // Set up union find sets
  for(Arrangement::Vertex_iterator vit = terrain.vertices_begin();
      vit!=terrain.vertices_end();
      vit++) {
    UFHv ufh=uf_0.make_set(vit);
    vertex_to_handle[vit]=ufh;
    UGraph_vertex_t node = boost::add_vertex(Graph_vertex(vit),M_delta);
    vertex_to_graph[vit]=node;
  }

  std::vector<Halfedge_handle> edges;
  for(Arrangement::Edge_iterator eit = terrain.edges_begin();
      eit!=terrain.edges_end();
      eit++) {
    edges.push_back(eit);
  }
  std::sort(edges.begin(),edges.end(),compare);

  for(int i=0;i<edges.size();i++) {
    Halfedge_handle e = edges[i];
    NT death = e->data().val;
    //if(verbose) std::cout << "Next edge to handle, Value: " << edges[i]->data().val << ": " << edges[i]->source()->point() << " - " << edges[i]->target()->point()<< std::endl;
    Vertex_handle v=e->source();
    Vertex_handle w=e->target();
    UFHv hv=uf_0.find(vertex_to_handle[v]);
    UFHv hw=uf_0.find(vertex_to_handle[w]);
    if(hv==hw) {
      continue;
    }
    Vertex_handle vertex_to_die = (*hv)->data().val < (*hw)->data().val ? *hw : *hv; 
    Vertex_handle vertex_to_persist = (*hv)->data().val < (*hw)->data().val ? *hv : *hw; 
    NT birth = vertex_to_die->data().val;
    
    if(death-birth<=2*delta) {
      boost::add_edge(vertex_to_graph[v],vertex_to_graph[w],UGraph_edge(e),M_delta);
      //if(verbose) std::cout << "Adding edge " << v->point() << " - " << w->point() << std::endl;
      uf_0.unify_sets(hv,hw);
      UFHv hnew = uf_0.find(hv);
      *hnew = vertex_to_persist;
    }
   
  }
  
  for(Arrangement::Vertex_iterator vit = terrain.vertices_begin();
      vit!=terrain.vertices_end();
      vit++) {
    UFHv ufh=uf_0.find(vertex_to_handle[vit]);
    R.insert(vertex_to_graph[*ufh]);
  }
  
  // Set up union sets in dim 1 using triangles
  for(Arrangement::Face_iterator fit = terrain.faces_begin();
      fit!=terrain.faces_end();
      fit++) {
    if(fit->is_unbounded()) {
      UFHf ufh=uf_1.make_set(fit);
      face_to_handle[fit]=ufh;
      
    } else {
      UFHf ufh=uf_1.make_set(fit);
      face_to_handle[fit]=ufh;
    }
    UGraph_vertex_t node = boost::add_vertex(Graph_vertex(fit),M_delta);
    face_to_graph[fit]=node; 
  }  
  
  // Compute persistence in dim 1 
  for(int i=edges.size()-1;i>=0;i--) {
    //if(verbose) std::cout << "Value: " << edges[i]->data().val << ": " << edges[i]->source()->point() << " - " << edges[i]->target()->point()<< std::endl;
    Halfedge_handle e = edges[i];
    NT birth = e->data().val;
    Face_handle f = e->face();
    Face_handle g = e->twin()->face();

    UFHf hf=uf_1.find(face_to_handle[f]);
    UFHf hg=uf_1.find(face_to_handle[g]);
    if(hf==hg) {
      continue;
    }
    Face_handle face_to_die = ((*hf)->data().val < (*hg)->data().val) ? *hf : *hg;
    Face_handle face_to_persist = ((*hf)->data().val < (*hg)->data().val) ? *hg : *hf;
    NT death = face_to_die->data().val;
    CGAL_assertion(birth<=death);
    if(death-birth<=2*delta) {
      boost::add_edge(face_to_graph[f],face_to_graph[g],UGraph_edge(e),M_delta);
      //if(verbose) std::cout << "Adding edge " << e->source()->point() << " - " << e->target()->point() << std::endl;
      uf_1.unify_sets(hf,hg);
      UFHf hnew = uf_1.find(hf);
      *hnew = face_to_persist;
    }
  }
  for(Arrangement::Face_iterator fit = terrain.faces_begin();
      fit!=terrain.faces_end();
      fit++) {
    UFHf ufh=uf_1.find(face_to_handle[fit]);
    R.insert(face_to_graph[*ufh]);
  }

}

template<typename Vertex, typename Graph>
void print_node_info(Vertex node,Graph& digraph) {
  int dim=digraph[node].dim;
  if(dim==0) {
    std::cout << "Node is point " << digraph[node].v->point() << std::endl;
  }
  if(dim==1) {
    std::cout << "Node is edge " << digraph[node].e->source()->point() << " - " << digraph[node].e->target()->point() << std::endl;
  }
  if(dim==2) {
    Face_handle f = digraph[node].f;
    if(f->is_unbounded()) {
      std::cout << "Node is unbounded face" << std::endl;
    } else {
      Arrangement::Ccb_halfedge_circulator run=f->outer_ccb();
      Vertex_handle a =run->source();
      run++;
      Vertex_handle b =run->source();
      run++;
      Vertex_handle c =run->source();
      run++;
      std::cout << "Node is face " << a->point() << " - " << b->point() << " - " << c->point() << std::endl;
    }
  }
}


template<typename VertexIterator>
void compute_gradient_field_and_hasse(Arrangement& terrain,
				      UGraph& M_delta,
				      VertexIterator births_begin, 
				      VertexIterator births_end,
				      DiGraph& digraph) {
  std::unordered_map<Vertex_handle,DiGraph_vertex_t> vertex_to_graph;
  std::unordered_map<Halfedge_handle,DiGraph_vertex_t> edge_to_graph;
  std::unordered_map<Face_handle,DiGraph_vertex_t> face_to_graph;

  for(Arrangement::Vertex_iterator vit = terrain.vertices_begin();
      vit!=terrain.vertices_end();
      vit++) {
    DiGraph_vertex_t node = boost::add_vertex(Graph_vertex(vit),digraph);
    vertex_to_graph[vit]=node;
  }
  for(Arrangement::Edge_iterator eit = terrain.edges_begin();
      eit!=terrain.edges_end();
      eit++) {
    DiGraph_vertex_t node = boost::add_vertex(Graph_vertex(eit),digraph);
    edge_to_graph[eit]=node;
    edge_to_graph[eit->twin()]=node;
  }
  for(Arrangement::Face_iterator fit = terrain.faces_begin();
      fit!=terrain.faces_end();
      fit++) {
    UGraph_vertex_t node = boost::add_vertex(Graph_vertex(fit),digraph);
    face_to_graph[fit]=node;
  }
  std::stack<UGraph_vertex_t> stack;
  for(VertexIterator run=births_begin;run!=births_end;run++) {
    //if(verbose) std::cout << "Seed element: ";
    //if(verbose) print_node_info(*run,M_delta);
    stack.push(*run);
  }
  while(! stack.empty()) {
    UGraph_vertex_t node=stack.top();
    //if(verbose) std::cout << "Next node in M_delta ";
    //if(verbose) print_node_info(node,M_delta);
    stack.pop();
    if(! M_delta[node].visited) {
      M_delta[node].visited=true;
      boost::graph_traits<UGraph>::out_edge_iterator out, out_end;
      boost::tie(out,out_end)=boost::out_edges(node,M_delta);
      while(out!=out_end) {
	CGAL_assertion(boost::source(*out,M_delta)==node);
	UGraph_vertex_t target = boost::target(*out,M_delta);
	if(! M_delta[target].visited) {
	  Halfedge_handle eh = M_delta[*out].e;
	  int dim_of_node = M_delta[node].dim;
	  if(dim_of_node==0) {
	    //if(verbose) std::cout << "Create directed edge:" << std::endl;
	    //if(verbose) print_node_info(vertex_to_graph[M_delta[target].v],digraph);
	    //if(verbose) print_node_info(edge_to_graph[eh],digraph);
	    //if(verbose) std::cout << "---" << std::endl;
	    boost::add_edge(vertex_to_graph[M_delta[target].v],edge_to_graph[eh],digraph);
	  } else {
	    //if(verbose) std::cout << "Create directed edge:" << std::endl;
	    //if(verbose) print_node_info(edge_to_graph[eh],digraph);
	    //if(verbose) print_node_info(face_to_graph[M_delta[target].f],digraph);
	    //if(verbose) std::cout << "---" << std::endl;
	    boost::add_edge(edge_to_graph[eh],face_to_graph[M_delta[target].f],digraph);
	  }
	  stack.push(target);
	}
	out++;
      }
    }
  }

  //if(verbose) std::cout << "At this point, there are " << boost::num_edges(digraph) << " directed edges" << std::endl;
  //if(verbose) std::cout << "Now extend to Hasse diagram" << std::endl;
  // Now extend the vector field to the Hasse diagram
  for(Arrangement::Edge_iterator eit = terrain.edges_begin();
      eit!=terrain.edges_end();
      eit++) {
    DiGraph_vertex_t enode = edge_to_graph[eit];

    Vertex_handle v = eit->source();
    Vertex_handle w = eit->target();
    DiGraph_vertex_t vnode = vertex_to_graph[v];
    DiGraph_vertex_t wnode = vertex_to_graph[w];

    if(! boost::edge(vnode,enode,digraph).second) {
      //if(verbose) std::cout << "Create directed edge:" << std::endl;
      //if(verbose) print_node_info(enode,digraph);
      //if(verbose) print_node_info(vnode,digraph);
      //if(verbose) std::cout << "---" << std::endl;
      boost::add_edge(enode,vnode,digraph);
    }
    if(! boost::edge(wnode,enode,digraph).second) {
      //if(verbose) std::cout << "Create directed edge:" << std::endl;
      //if(verbose) print_node_info(enode,digraph);
      //if(verbose) print_node_info(wnode,digraph);
      //if(verbose) std::cout << "---" << std::endl;
      boost::add_edge(enode,wnode,digraph);
    }
    Face_handle f = eit->face();
    Face_handle g = eit->twin()->face();
    
    DiGraph_vertex_t fnode = face_to_graph[f];
    DiGraph_vertex_t gnode = face_to_graph[g];
    
    if(! boost::edge(enode,fnode,digraph).second) {
      //if(verbose) std::cout << "Create directed edge:" << std::endl;
      //if(verbose) print_node_info(fnode,digraph);
      //if(verbose) print_node_info(enode,digraph);
      //if(verbose) std::cout << "---" << std::endl;
      boost::add_edge(fnode,enode,digraph);
    }
    if(! boost::edge(enode,gnode,digraph).second) {
      //if(verbose) std::cout << "Create directed edge:" << std::endl;
      //if(verbose) print_node_info(gnode,digraph);
      //if(verbose) print_node_info(enode,digraph);
      //if(verbose) std::cout << "---" << std::endl;
      boost::add_edge(gnode,enode,digraph);
    }
  }
  //if(verbose) std::cout << "Digraph has now " << boost::num_vertices(digraph) << " nodes and " << boost::num_edges(digraph) << " edges" << std::endl;
  
}

template<typename DiGraphType>
NT get_data_of_node(DiGraph_vertex_t node,DiGraphType& digraph) {
  int dim=digraph[node].dim;
  if(dim==0) {
    return digraph[node].v->data().val;
  }
  if(dim==1) {
    return digraph[node].e->data().val;
  }
  if(dim==2) {
    return digraph[node].f->data().val;
  }
  return NT(0); //to supress warning
}

template<typename DiGraphType>
void set_data_of_node(DiGraph_vertex_t node,DiGraphType& digraph,NT val) {
  int dim=digraph[node].dim;
  if(dim==0) {
    digraph[node].v->data().val = val;
  }
  if(dim==1) {
    digraph[node].e->data().val = val;
    digraph[node].e->twin()->data().val = val;
  }
  if(dim==2) {
    digraph[node].f->data().val = val;
  }
}

template<typename DiGraphType>
NT _compute_function(DiGraph_vertex_t node, DiGraphType& digraph,NT delta) {
  NT diff=0.0;
  if(! digraph[node].visited) {
    digraph[node].visited=true;
    //if(verbose) std::cout << "Now visiting: ";
    //if(verbose) print_node_info(node,digraph);
    typename boost::graph_traits<DiGraphType>::out_edge_iterator out, out_end;
    boost::tie(out,out_end)=boost::out_edges(node,digraph);
    while(out!=out_end) {
      DiGraph_vertex_t target = boost::target(*out,digraph);
      NT curr_diff = _compute_function(target,digraph,delta);
      diff=std::max(diff,curr_diff);
      out++;
    }
    //if(verbose) std::cout << "Now assigning: ";
    //if(verbose) print_node_info(node,digraph);
    NT old_data = get_data_of_node(node,digraph);
    //if(verbose) std::cout << "Old data is " << old_data << std::endl;
    set_data_of_node(node,digraph,old_data-delta);
    //if(verbose) std::cout << "Now set to " << get_data_of_node(node,digraph) << std::endl;
    boost::tie(out,out_end)=boost::out_edges(node,digraph);
    while(out!=out_end) {
      DiGraph_vertex_t target=boost::target(*out,digraph);
      //if(verbose) std::cout << "Successor: ";
      //if(verbose) print_node_info(target,digraph);
      set_data_of_node(node,digraph,std::max(get_data_of_node(node,digraph),
					     get_data_of_node(target,digraph)));
      //if(verbose) std::cout << "Now set to " << get_data_of_node(node,digraph) << std::endl;
      out++;
    }
    diff=std::max(diff,CGAL::abs(old_data-get_data_of_node(node,digraph)));
  }
  return diff;
}



template<typename DiGraphType>
NT compute_function(Arrangement& terrain, DiGraphType& digraph,NT delta) {
  // Mark all unvisited first
  {
    typename boost::graph_traits<DiGraphType>::vertex_iterator node_it,node_end;
    boost::tie(node_it,node_end)=boost::vertices(digraph);
    while(node_it!=node_end) {
      digraph[*node_it].visited=false;
      node_it++;
    }
  }

  typename boost::graph_traits<DiGraphType>::vertex_iterator node_it,node_end;
  boost::tie(node_it,node_end)=boost::vertices(digraph);
  NT diff=0.0;
  while(node_it!=node_end) {
    NT new_diff = _compute_function(*node_it,digraph,delta);
    diff=std::max(diff,new_diff);
    node_it++;
  }
  return diff;
  
}





struct Data_cache {

  std::unordered_map<Vertex_handle,NT> vertex_data;
  std::unordered_map<Halfedge_handle,NT> edge_data;
  std::unordered_map<Face_handle,NT> face_data;
  
  void store(Arrangement& terrain) {
    vertex_data.clear();
    edge_data.clear();
    face_data.clear();
    for(Arrangement::Vertex_iterator vit = terrain.vertices_begin();
	vit!=terrain.vertices_end();
	vit++) {
      vertex_data[vit]=vit->data().val;
    }
    for(Arrangement::Halfedge_iterator eit = terrain.halfedges_begin();
	eit!=terrain.halfedges_end();
	eit++) {
      edge_data[eit]=eit->data().val;
    }
    for(Arrangement::Face_iterator fit = terrain.faces_begin();
	fit!=terrain.faces_end();
	fit++) {
      face_data[fit]=fit->data().val;
    }
  }
  
  void reset(Arrangement& terrain) {
    for(Arrangement::Vertex_iterator vit = terrain.vertices_begin();
	vit!=terrain.vertices_end();
	vit++) {
      vit->data().val=vertex_data[vit];
    }
    for(Arrangement::Halfedge_iterator eit = terrain.halfedges_begin();
	eit!=terrain.halfedges_end();
	eit++) {
      eit->data().val=edge_data[eit];
    }
    for(Arrangement::Face_iterator fit = terrain.faces_begin();
	fit!=terrain.faces_end();
	fit++) {
      fit->data().val=face_data[fit];
    }
  }
  
  NT& get_data(Vertex_handle v) {
    return vertex_data[v];
  }
  NT& get_data(Halfedge_handle v) {
    return edge_data[v];
  }
  NT& get_data(Face_handle v) {
    return face_data[v];
  }
  
};

void print_terrain_data(Arrangement& terrain,std::string prefix="", Data_cache cache=Data_cache(),bool print_diff=false) {

  std::cout << prefix << std::endl;
  
  for(Arrangement::Vertex_iterator vit = terrain.vertices_begin();
      vit!=terrain.vertices_end();
      vit++) {
    NT val = print_diff ? CGAL::abs(vit->data().val - cache.get_data(vit)) : vit->data().val;
    std::cout << vit->point() << " -> " << val << std::endl;
  }
  for(Arrangement::Edge_iterator eit = terrain.edges_begin();
      eit!=terrain.edges_end();
      eit++) {
    NT val = print_diff ? CGAL::abs(eit->data().val - cache.get_data(eit)) : eit->data().val;
    std::cout << eit->source()->point() << " - " << eit->target()->point() << " -> " << val << std::endl;
  }
  for(Arrangement::Face_iterator fit = terrain.faces_begin();
      fit!=terrain.faces_end();
      fit++) {
    if(fit->is_unbounded()) {
      continue;
    }
    NT val = print_diff ? CGAL::abs(fit->data().val - cache.get_data(fit)) : fit->data().val;
    Arrangement::Ccb_halfedge_circulator run=fit->outer_ccb();
    Vertex_handle a =run->source();
    run++;
    Vertex_handle b =run->source();
    run++;
    Vertex_handle c =run->source();
    run++;
    std::cout << a->point() << " - " << b->point() << " - " << c->point() << ": " << val << std::endl;    
  }
}

// Assumption is that both arrangements are equal except for the data-entries
NT L_infty_distance_filtrations(Arrangement& arr1,Arrangement& arr2) {
  NT diff=0.0;
    CGAL_assertion_code(int n=arr1.number_of_vertices());
    CGAL_assertion(arr2.number_of_vertices()==n);
  for(Arrangement::Vertex_iterator vit = arr1.vertices_begin();
      vit!=arr1.vertices_end();
      vit++) {
    Vertex_handle vc = CGAL::insert_point(arr2,vit->point());
    CGAL_assertion(vit->degree()==vc->degree());
    CGAL_assertion(arr2.number_of_vertices()==n);
    diff=std::max(diff,CGAL::abs(vit->data().val-vc->data().val));
    Arrangement::Halfedge_around_vertex_circulator e1=vit->incident_halfedges(),start1=e1,
      e2=vc->incident_halfedges();
    while(e2->source()->point()!=e1->source()->point()) {
      e2++;
    }
    do {
      diff=std::max(diff,CGAL::abs(e1->data().val-e2->data().val));
      diff=std::max(diff,CGAL::abs(e1->face()->data().val-e2->face()->data().val));
      e1++;
      e2++;
    } while(e1!=start1);
    
  }

  return diff;
}

NT L_infty_distance_filtrations(Arrangement& terrain,Data_cache& cache) {
  NT diff=0.0;

  for(Arrangement::Vertex_iterator vit = terrain.vertices_begin();
      vit!=terrain.vertices_end();
      vit++) {
    diff=std::max(diff,CGAL::abs(vit->data().val - cache.get_data(vit)));
  }
  for(Arrangement::Halfedge_iterator eit = terrain.halfedges_begin();
      eit!=terrain.halfedges_end();
      eit++) {
    diff=std::max(diff,CGAL::abs(eit->data().val - cache.get_data(eit)));
  }
  for(Arrangement::Face_iterator fit = terrain.faces_begin();
      fit!=terrain.faces_end();
      fit++) {
    diff=std::max(diff,CGAL::abs(fit->data().val - cache.get_data(fit)));
  }

  return diff;
}


Point place_barycenter(Face_handle fit,NT precision,Data_cache& cache) {
  Arrangement::Ccb_halfedge_circulator run=fit->outer_ccb();
  Vertex_handle a = run->source();
  run++;
  Vertex_handle b = run->source();
  run++;
  Vertex_handle c = run->source();
  run++;

  // Store the data of points
  NT adata=a->data().val,bdata=b->data().val,cdata=c->data().val;
  // Now re-set the data values temporarily. That allows the usage
  // of the elevation_in_triangle function
  a->data().val = cache.get_data(a);
  b->data().val = cache.get_data(b);
  c->data().val = cache.get_data(c);
  
  if(a->data().val<b->data().val) {
    std::swap(a,b);
    std::swap(adata,bdata);
  }
  if(a->data().val<c->data().val) {
    std::swap(a,c);
    std::swap(adata,cdata);
    
  }
  CGAL_assertion(a->data().val>=b->data().val);
  CGAL_assertion(a->data().val>=c->data().val);
  
  NT lambda;
  if(CGAL::is_zero(a->data().val-(b->data().val+c->data().val)/2)) {
    lambda=NT(1./3);
  } else {
    lambda = (fit->data().val-precision-(b->data().val+c->data().val)/2)/(a->data().val-(b->data().val+c->data().val)/2);
  }

  //if(verbose) std::cout << "Computed lambda is " << lambda << std::endl;

  lambda=std::max(NT(1./3),lambda);

  if(lambda>=1) {
    std::cerr << "A bad delta is chosen: the blw algorithm cannot find a subdivision point that satisfies the L_infty constraint. Please try with another delta" << std::endl;
    std::exit(1);
  }
  
  CGAL_assertion(lambda<1);
  NT mu=(1-lambda)/2;

  Point ap=a->point();
  Point bp=b->point();
  Point cp=c->point();

  Point bary(lambda*ap.x()+mu*bp.x()+mu*cp.x(),
	     lambda*ap.y()+mu*bp.y()+mu*cp.y());
  //if(verbose) std::cout << "Original values: " << a->data().val << " " << b->data().val << " " << c->data().val << std::endl;
  //if(verbose) std::cout << "Elevation in tri: " << elevation_in_triangle(bary,a,b,c) << ", face data: " << fit->data().val << " Prec: " << precision << std::endl;
  //if(verbose) std::cout << "Direct computation: " << lambda*a->data().val+mu*b->data().val+mu*c->data().val << std::endl;
  CGAL_assertion(CGAL::abs(elevation_in_triangle(bary,a,b,c)-fit->data().val)<=precision);
  // Undo the data changes
  a->data().val = adata;
  b->data().val = bdata;
  c->data().val = cdata;
  return bary;
}

Point place_barycenter(Halfedge_handle eit,NT precision,Data_cache& cache) {
  Vertex_handle a = eit->source();
  Vertex_handle b = eit->target();
  NT adata=a->data().val;
  NT bdata=b->data().val;
  a->data().val = cache.get_data(a);
  b->data().val = cache.get_data(b);
  
  if(a->data().val<b->data().val) {
    std::swap(a,b);
    std::swap(adata,bdata);
  }

  Point ap=a->point();
  Point bp=b->point();

  NT lambda = CGAL::is_zero(a->data().val-b->data().val) ? NT(0.5) : (eit->data().val-precision-b->data().val)/(a->data().val-b->data().val);
  lambda=std::max(NT(0.5),lambda);

  //if(verbose) std::cout << "Halfedge lambda: " << lambda << std::endl;

  if(lambda>=1) {
    std::cerr << "A bad delta is chosen: the blw algorithm cannot find a subdivision point that satisfies the L_infty constraint. Please try with another delta" << std::endl;
    std::exit(1);
  }

  CGAL_assertion(lambda<1);
  
  Point bary(lambda*ap.x()+(1-lambda)*bp.x(),
	     lambda*ap.y()+(1-lambda)*bp.y());
  
  //CGAL_assertion(CGAL::abs(elevation_on_edge(bary,a,b)-eit->data())<=precision);

  a->data().val=adata;
  b->data().val=bdata;

  return Point(lambda*ap.x()+(1-lambda)*bp.x(),
	       lambda*ap.y()+(1-lambda)*bp.y());
}
    
void convert_to_pl(Arrangement& terrain,NT precision,Data_cache& cache, bool avoid_subdivision) {
  std::cout << "Initially, is triangulation? " << (is_triangulation(terrain) ? "Yes" : "No") << std::endl;
  std::cout << "Precision is " << precision << std::endl;
  // Create isolated points
  std::cout << "Create isol points" << std::endl;
  
  CGAL::Arr_accessor<Arrangement> accessor(terrain);

  std::vector<Halfedge_handle> he_handles;
  std::set<Face_handle> faces_to_subdivide;
  for(Arrangement::Edge_iterator eit = terrain.edges_begin();
      eit!=terrain.edges_end();
      eit++) {
    if(avoid_subdivision) {
      NT& data_of_edge=eit->data().val;
      Vertex_handle v = eit->source();
      Vertex_handle w = eit->target();
      NT& input_data_v = v->data().val;
      NT& input_data_w = w->data().val;
      if( std::max(input_data_v,input_data_w)==data_of_edge ) {
	continue;
      }      
    }
      
    he_handles.push_back(eit);
    faces_to_subdivide.insert(eit->face());
    faces_to_subdivide.insert(eit->twin()->face());
    
      
  }

  for(Arrangement::Face_iterator fit = terrain.faces_begin();
      fit!=terrain.faces_end();
      fit++) {
    if(fit->is_unbounded()) {
      continue;
    }
    NT data_of_face=fit->data().val;
    if(avoid_subdivision) {
      if(faces_to_subdivide.find(fit)==faces_to_subdivide.end()) {
	
	Arrangement::Ccb_halfedge_circulator run=fit->outer_ccb();
	Vertex_handle a = run->source();
	run++;
	Vertex_handle b = run->source();
	run++;
	Vertex_handle c = run->source();
	run++;
	NT max_input = std::max ( std::max(a->data().val,b->data().val),c->data().val );
	if( data_of_face==max_input) {
	  continue;
	}
      }
    }
    Point barycenter = place_barycenter(fit,precision,cache);
    CGAL::Object hint = fit->data().hint;
    // Use internal arrangement functions to avoid point location
    Vertex_handle new_v = accessor.create_vertex(barycenter);
    new_v->set_data(Data(data_of_face,hint));
    accessor.insert_isolated_vertex(fit,new_v);
  }
  // Split edges
  std::cout << "Split edges" << std::endl;

  for(int i=0;i<he_handles.size();i++) {
    Halfedge_handle eit=he_handles[i];
    CGAL::Object hint = eit->data().hint;
    NT data_of_edge=eit->data().val;
    Point barycenter=place_barycenter(eit,precision,cache);
    Halfedge_handle new_e = terrain.split_edge(eit,barycenter);
    CGAL_assertion(new_e->source()->point()==eit->source()->point());
    new_e->target()->set_data(Data(data_of_edge,hint));
  }
  // Connect inside face
  std::cout << "Create segments" << std::endl;
  std::vector<Segment> segments;
  Construct_segment construct_segment;
  for(Arrangement::Face_iterator fit = terrain.faces_begin();
      fit!=terrain.faces_end();
      fit++) {
    if(fit->is_unbounded()) {
      continue;
    }
    CGAL_assertion(std::distance(fit->isolated_vertices_begin(),fit->isolated_vertices_end())<=1);
    CGAL_assertion(avoid_subdivision || 
		   std::distance(fit->isolated_vertices_begin(),fit->isolated_vertices_end())==1);
    if(fit->isolated_vertices_begin()==fit->isolated_vertices_end()) {
      continue;
    }
    Point bary = fit->isolated_vertices_begin()->point();
    Arrangement::Ccb_halfedge_circulator run=fit->outer_ccb(),start=run;
    int count=0;
    do {
      Point& on_bd = run->source()->point();
      segments.push_back(construct_segment(on_bd,bary));
      count++;
      run++;
    } while(run!=start);
    CGAL_assertion(count<=6);
    CGAL_assertion(avoid_subdivision || count==6);
    CGAL_assertion(run==start);
  }
  std::cout << "Obtained " << segments.size() << " segments to add" << std::endl;
  CGAL::insert_non_intersecting_curves(terrain,segments.begin(),segments.end());
  std::cout << "Done, is triangulation? " << (is_triangulation(terrain) ? "Yes" : "No") << std::endl;
  std::cout << "Set lower_star_data" << std::endl;
  set_lower_star_data(terrain);
}

void negate_values(Arrangement& terrain) {
  
  for(Arrangement::Vertex_iterator vit = terrain.vertices_begin();
      vit!=terrain.vertices_end();
      vit++) {
    vit->data().val=-vit->data().val;
  }
  for(Arrangement::Halfedge_iterator eit = terrain.halfedges_begin();
      eit!=terrain.halfedges_end();
      eit++) {
    eit->data().val = -eit->data().val;
  }
  for(Arrangement::Face_iterator fit = terrain.faces_begin();
      fit!=terrain.faces_end();
      fit++) {
    fit->data().val = -fit->data().val;
  }
}

void average_values(Arrangement& terrain,Data_cache& cache) {
  
  for(Arrangement::Vertex_iterator vit = terrain.vertices_begin();
      vit!=terrain.vertices_end();
      vit++) {
    vit->data().val = (vit->data().val+cache.get_data(vit))/2;
  }
  for(Arrangement::Halfedge_iterator eit = terrain.halfedges_begin();
      eit!=terrain.halfedges_end();
      eit++) {
    eit->data().val = (eit->data().val+cache.get_data(eit))/2;
  }
  for(Arrangement::Face_iterator fit = terrain.faces_begin();
      fit!=terrain.faces_end();
      fit++) {
    fit->data().val = (fit->data().val + cache.get_data(fit))/2;
  }
}


// The Bauer-Lange-Wardetzky algorithm
// Taken from the PhD thesis of Bauer
void topological_simplification(Arrangement& terrain, NT delta,bool avoid_subdivision=true) {

  CGAL_assertion_code(Arrangement copy(terrain));

  std::cout << "Simplification" << std::endl;
  
  blw_pure_timer.start();
  
  // Store data values for later use
  Data_cache cache;
  cache.store(terrain);

  //if(verbose) print_terrain_data(terrain,"Before");
  UGraph ugraph;
  std::set<UGraph_vertex_t> births;
  
  simplification_low_pers_pairs(terrain,delta,ugraph,births);
  //if(verbose) std::cout << "UGraph has " << boost::num_vertices(ugraph) << " vertices and " << boost::num_edges(ugraph) << " edges" << std::endl;
  //if(verbose) std::cout << "births has " << births.size() << " elements" << std::endl;
  DiGraph digraph;

  compute_gradient_field_and_hasse(terrain,ugraph,births.begin(),births.end(),digraph);
  
  compute_function(terrain,digraph,delta);
  
  //if(verbose) print_terrain_data(terrain,"After 1st");

  Data_cache cache_1st_try;
  cache_1st_try.store(terrain);
  
  cache.reset(terrain);
  negate_values(terrain);
  DiGraph rev_digraph;

  auto rev_graph = boost::make_reverse_graph(digraph);

  NT diff_for_0 = compute_function(terrain,rev_graph,delta);

  //if(verbose) std::cout << "Difference after first try: " << diff_for_0 << std::endl;

  negate_values(terrain);

  //if(verbose) print_terrain_data(terrain,"After 2nd");

  average_values(terrain,cache_1st_try);

  //if(verbose) print_terrain_data(terrain,"Final");

  //if(verbose) print_terrain_data(terrain,"Difference",cache,true);
  
  NT diff = L_infty_distance_filtrations(terrain,cache);

  std::cout << "Difference is " << diff << std::endl;
  
  /*
  {
    Persistence_diagram pers_0,pers_1;
    compute_persistence(terrain,pers_0,pers_1);
    print_persistence_diagram(pers_0,"Simplified Diagram 0:");
    print_persistence_diagram(pers_1,"Simplified Diagram 1:");
  }

  cache.reset(terrain);
 
  {
    Persistence_diagram pers_0,pers_1;
    compute_persistence(terrain,pers_0,pers_1);
    print_persistence_diagram(pers_0,"Original Diagram 0:");
    print_persistence_diagram(pers_1,"Original Diagram 1:");
  }

  std::exit(1);

  */

  CGAL_assertion(diff<=delta);

  //if(verbose) print_terrain_data(terrain,"After");
  CGAL_assertion_code(std::cout << "L_infty distance: " << L_infty_distance_filtrations(terrain,copy) << std::endl);

  blw_pure_timer.stop();

  convert_to_pl_timer.start();

  //NT tolerance = (delta>diff ? (delta+diff)/2 : diff*11./10);
  //NT tolerance = (delta>diff ? delta : delta*11./10);
  NT tolerance=delta;
  std::cout << "Diff tolerance delta: " << diff << " " << tolerance << " " << delta << std::endl;
  convert_to_pl(terrain,tolerance,cache,avoid_subdivision);

  convert_to_pl_timer.stop();

  //print_terrain_data(terrain, "As PL-terrain:");

}

////////////////////////////////////////////////////////////////////////////////

// The inheritance is just for lazyness (many funtions of Link_retriangulator can be re-used)
// TODO: Make the two classes independent and let them derive from a common base class
class Edge_checker : public Link_retriangulator {

  Halfedge_handle eh;
  Vertex_handle vp,vq,vr,vs;

public:

  Edge_checker(Arrangement& base_terrain)
    : Link_retriangulator(base_terrain)
  {}

  bool convex_link() {
    Kernel::Orientation_2 orient;
    
    if( orient(vs->point(),vq->point(),vr->point())!=CGAL::LEFT_TURN) {
      return false;
    }
    
    return orient(vr->point(),vp->point(),vs->point());
  }

  bool convex_hull_edge() {
    return eh->face()->is_unbounded() || eh->twin()->face()->is_unbounded();
  }

  bool flip_improves_angle() {

    return (CGAL::side_of_bounded_circle(vp->point(),vq->point(),vr->point(),vs->point())==CGAL::ON_BOUNDED_SIDE);
  }

  bool persistent_aware() {
    return ( std::max(vp->data().val,vq->data().val) >= std::min(vr->data().val,vs->data().val) ) &&
      ( std::max(vr->data().val,vs->data().val) >= std::min(vp->data().val,vq->data().val) );
  }

  bool L_infty_aware(NT delta) {
    //if(verbose) std::cout << "L_infty aware " << vr->point() << " - " << vs->point() << std::endl;
    NT diff_edge = compute_L_infty_distance(vr,vs);
    //if(verbose) std::cout << "Diff edge: " << diff_edge << std::endl;
    if(diff_edge>delta) {
      return false;
    }
    NT diff_tri_1 = check_triangle(vr,vs,vp);
    //if(verbose) std::cout << "Diff tri1: " << diff_tri_1 << std::endl;
    if(diff_tri_1>delta) {
      return false;
    }
    NT diff_tri_2 = check_triangle(vr,vs,vq);
    //if(verbose) std::cout << "Diff tri2: " << diff_tri_2 << std::endl;
    if(diff_tri_2>delta) {
      return false;
    }
    return true;
  }

  bool operator() (Halfedge_handle eh, NT& delta) {
    this->eh=eh;
    this->vp = eh->source();
    this->vq = eh->target();
    this->vr=eh->next()->target();
    this->vs= eh->twin()->next()->target();
    //if(verbose) std::cout << "Edge: " << eh->source()->point() << ", " << eh->source()->data().val << " -- " << eh->target()->point() << ", " << eh->target()->data().val << std::endl;
    //if(verbose) std::cout << "Nbrs: " << vr->point() << ", " << vr->data().val << " -- " << vs->point() << ", " << vs->data().val << std::endl;
    if(convex_hull_edge()) {
      //if(verbose) std::cout << "Is convex hull edge" << std::endl;
      return false;
    }
    if(! convex_link()) {
      //if(verbose) std::cout << "not convex link" << std::endl;
      return false;
    }
    if(! flip_improves_angle()) {
      //if(verbose) std::cout << "Flip would not improve angle measure" << std::endl;
      return false;
    }
    if(! persistent_aware()) {
      //if(verbose) std::cout << "Not persistent aware" << std::endl;
      return false;
    }
    if(! L_infty_aware(delta)) {
      //if(verbose) std::cout << "L_infty distance is too large" << std::endl;
      return false;
    }
    //if(verbose) std::cout << "Edge can be flipped" << std::endl;
    return true;
  }

};

class Greedy_edge_flipper {

protected:
  Arrangement& terrain;
  Edge_checker& checker;
  NT& delta;
  std::set<Halfedge_handle> edges_to_check;
  std::vector<Halfedge_handle> mirrored_edge_set;
  int number_of_attempts;

public:

  Greedy_edge_flipper(Arrangement& terrain, Edge_checker& checker, NT delta) 
    : terrain(terrain), checker(checker), delta(delta)
  {
    srand(time(NULL));
    for(Arrangement::Edge_iterator eit = terrain.edges_begin();
	eit!=terrain.edges_end();
	eit++) {
      edges_to_check.insert(eit);
      mirrored_edge_set.push_back(eit);
      CGAL_assertion(edges_to_check.size()==mirrored_edge_set.size());
    }
  }

  bool flip_next_edge() {
    bool element_found=false;

    number_of_attempts=0;

    while(!edges_to_check.empty() && !element_found) {
      
      CGAL_assertion(edges_to_check.size()==mirrored_edge_set.size());
      int r = rand()%mirrored_edge_set.size();
      //std::cout << "Random choice: " << r << " of " << mirrored_edge_set.size() << std::endl;
      Halfedge_handle& next = mirrored_edge_set[r];
      
      //std::cout << "Trying " << next->source()->point() <<  " " << next->target()->point() << std::endl;
      
      bool result = checker(next,delta);
      CGAL_assertion(checker(next->twin(),delta)==result);
      number_of_attempts++;

      if(result) {
	
	//if(verbose) std::cout << "Flipping " << next->source()->point() << " - " << next->target()->point() << std::endl;
	element_found=true;
	std::vector<Halfedge_handle> link;
	link.push_back(next->next());
	link.push_back(next->next()->next());
	link.push_back(next->twin()->next());
	link.push_back(next->twin()->next()->next());

	//if(verbose) std::cout << "Flipped edge is " << next->next()->target()->point() << " - " << next->twin()->next()->target()->point() << std::endl;

	Segment s(next->next()->target()->point(),next->twin()->next()->target()->point());

	Vertex_handle p = next->source();
	Vertex_handle q = next->target();

	terrain.remove_edge(next);
	Halfedge_handle eh = CGAL::insert_non_intersecting_curve(terrain,s);

	CGAL_assertion(is_triangulation(terrain));

	CGAL_assertion( !checker(eh,delta));
	
	CGAL_assertion(is_at_boundary(p) || p->degree()>=3);
	CGAL_assertion(is_at_boundary(q) || q->degree()>=3);

	for(int i=0;i<link.size();i++) {
	  if(edges_to_check.find(link[i])==edges_to_check.end() && edges_to_check.find(link[i]->twin())==edges_to_check.end()) {
	    //if(verbose) std::cout << "Reinsert edge " << std::endl;
	    edges_to_check.insert(link[i]);
	    mirrored_edge_set.push_back(link[i]);
	  }
	}
      }
      int elements_removed = edges_to_check.erase(next);
      CGAL_assertion(elements_removed==1);
      mirrored_edge_set[r]=mirrored_edge_set.back();
      mirrored_edge_set.pop_back();
    }

    //std::cout << "Number of attempts: " << number_of_attempts << std::endl;
    
    return element_found;
  }


};

////////////////////////////////////////////////////////////////////////////////

int main(int argc,char** argv) {

  namespace po = boost::program_options;

  po::options_description desc("Options");

  desc.add_options()
    ("help,h", "Help screen")
    ("input,i", po::value<std::string>(),"Input file")
    ("delta", po::value<double>(), "delta-value")
    ("with-blw", "Use the simplificication algorithm from Bauer et al")
    ("without-persistent-aware", "Don't check edges for persistent aware")
    ("without-L_infty-on-edges", "Don't check edges for L_infty aware")
    ("without-L_infty-on-triangles", "Don't check triangles for L_infty aware")
    ("remove-critical-points", "Allow removal of critical points")
    ("heap", "Use heap simplifier")
    ("greedy", "Use greedy simplifier")
    ("print-progress",po::value<int>(), "How often should a status message been printed?")
    ("only-write-input-obj-file", "Writes the input terrain in obj-file and exits")
    ("only-write-simplified-obj-file", "Writes the blw-simplified terrain in obj-file and exits")
    ("avoid-subdivision", "Tries to avoid subdivisions as much as possible (implies --with-blw)")
    ("flip", "After simplification, tries to flip edges to make domain 'more Delaunay'")
    ("filter-long-edges",po::value<double>(), "Remove edges from the Delaunay triangulation that are a multiple longer than the median edge");

  po::positional_options_description pos_desc;
  pos_desc.add("input", 1);
  pos_desc.add("delta", 1);

  po::command_line_parser parser(argc,argv);
  parser.options(desc).positional(pos_desc);

  po::variables_map vm;
  po::store(parser.run(), vm);

  if(vm.count("help")) {
    std::cout << desc << std::endl;
    return 0;
  }
  if(!vm.count("input")) {
    std::cout << "No input file specified" << std::endl;
    return 0;
  }
  std::string input_filename = vm["input"].as<std::string>();

  std::ifstream ifstr(input_filename.c_str());

  NT delta=0.1;
  if(vm.count("delta")) {
    delta=vm["delta"].as<double>();
  }

  bool use_simplification=false;
  if(vm.count("with-blw")) {
    use_simplification=true;
  }

  bool avoid_subdivision=false;
  if(vm.count("avoid-subdivision")) {
    avoid_subdivision=true;
    if(! use_simplification) {
      std::cout << "The option avoid-subdivision implies with-blw" << std::endl;
      use_simplification=true;
    }
  }
  
  if(vm.count("only-write-input-obj-file") && vm.count("only-write-simplified-obj-file")) {
    std::cout << "Options --only-write-input-obj-file and --only-write-simplified-obj-file"
	      << " cannot both be used"
	      << std::endl;
  }
  if(vm.count("only-write-simplified-obj-file") && !use_simplification) {
    use_simplification=true;
    std::cout << "The option only-write-simplified-obj-file implies with-blw" << std::endl;
  }

  bool mind_persistent_aware=true;
  if(vm.count("without-persistent-aware")) {
    mind_persistent_aware=false;
  }

  bool keep_critical_points=true;
  if(vm.count("remove-critical-points")) {
    keep_critical_points=false;
  }

  bool mind_L_infty_on_edges=true;
  if(vm.count("without-L_infty-on-edges")) {
    mind_L_infty_on_edges=false;
  }

  bool mind_L_infty_on_triangles=true;
  if(vm.count("without-L_infty-on-triangles")) {
    mind_L_infty_on_triangles=false;
  }

  if(mind_persistent_aware && !keep_critical_points) {
    std::cout << "The option remove-critical-points implies without-persistent-aware" << std::endl;
    mind_persistent_aware=false;
  }

  int print_progress=std::numeric_limits<int>::max();
  if(vm.count("print-progress")) {
    print_progress=vm["print-progress"].as<int>();
  }

  bool use_heap_simplifier=false;
  if(vm.count("greedy")) {
    use_heap_simplifier=false;
  }
  if(vm.count("heap")) {
    use_heap_simplifier=true;
  }
  if(vm.count("greedy") && vm.count("heap")) {
    std::cerr << "Options --heap and --greedy are mutually exclusive" << std::endl;
    std::exit(1);
  }

  bool do_flips=false;
  if(vm.count("flip")) {
    do_flips=true;
  }

  bool filter_long_edges=false;
  double filter_threshold=std::numeric_limits<double>::max();
  if(vm.count("filter-long-edges")) {
    filter_long_edges=true;
    filter_threshold=vm["filter-long-edges"].as<double>();
  }

  initialize_timers();
  overall_timer.start();
  algorithm_timer.start();

  std::vector<std::pair<Point,NT> > points_with_elevation;
  std::cout << "Reading input" << std::endl;
  read_points(ifstr,std::back_inserter(points_with_elevation));
  
  Arrangement input_terrain;
  Ric_point_location point_location_input;
  std::cout << "Create arrangement" << std::endl;
  create_arrangement_timer.start();
  create_arrangement(input_terrain,point_location_input,filter_long_edges,filter_threshold,points_with_elevation.begin(),points_with_elevation.end());
  create_arrangement_timer.stop();

  CGAL_assertion(is_triangulation(input_terrain));

  std::cout << "Number of vertices: " << input_terrain.number_of_vertices() << std::endl;
  std::cout << "Number of edges: " << input_terrain.number_of_edges() << std::endl;
  std::cout << "Number of faces: " << input_terrain.number_of_faces() << std::endl;
  std::cout << "Number of unbounded faces: " << input_terrain.number_of_unbounded_faces() << std::endl;
  std::cout << "Number of critical points: " << count_critical_points(input_terrain) << std::endl;

  std::cout << "Total number of finite cells: " << input_terrain.number_of_vertices()+input_terrain.number_of_edges()+input_terrain.number_of_faces()-1 << std::endl;


  algorithm_timer.stop();

  write_to_obj_file(input_terrain,input_filename,"input");
  if( vm.count("only-write-input-obj-file") ) {
    return 0;
  }

  {
    Persistence_diagram pers_0,pers_1;
    compute_persistence(input_terrain,pers_0,pers_1);
    int count_pers_0=0;
    int count_pers_1=0;
    for(int i=0;i<pers_0.size();i++) {
      if(pers_0[i].second-pers_0[i].first>2*delta) {
	count_pers_0++;
      }
    }
    for(int i=0;i<pers_1.size();i++) {
      if(pers_1[i].second-pers_1[i].first>2*delta) {
	count_pers_1++;
      }
    }
    std::cout << "Original diagram with persistence pairs>2delta in dim 0: " << count_pers_0 << std::endl;
    std::cout << "Original diagram with persistence pairs>2delta in dim 1: " << count_pers_1 << std::endl;

    //print_persistence_diagram(pers_0,"Diagram 0:");
    //print_persistence_diagram(pers_1,"Diagram 1:");
    
  }
  
  algorithm_timer.resume();

  Arrangement simplified_terrain(input_terrain);

  algorithm_timer.stop();

  for(Arrangement::Vertex_iterator vit = simplified_terrain.vertices_begin();
      vit!=simplified_terrain.vertices_end();
      vit++) {
    CGAL_assertion(vit->data().hint.is<Vertex_const_handle>());
  }
  for(Arrangement::Edge_iterator eit = simplified_terrain.edges_begin();
      eit!=simplified_terrain.edges_end();
      eit++) {
    CGAL_assertion(eit->data().hint.is<Halfedge_const_handle>());
    CGAL_assertion(eit->twin()->data().hint.is<Halfedge_const_handle>());
  }
  for(Arrangement::Face_iterator fit = simplified_terrain.faces_begin();
      fit!=simplified_terrain.faces_end();
      fit++) {
    CGAL_assertion(fit->data().hint.is<Face_const_handle>());
  }


  if(use_simplification) {
    algorithm_timer.resume();
    blw_timer.start();
    topological_simplification(simplified_terrain,delta,avoid_subdivision);
    blw_timer.stop();
    algorithm_timer.stop();

    std::cout << "Terrain after simplification: " << std::endl;
    std::cout << "Number of vertices: " << simplified_terrain.number_of_vertices() << std::endl;
    std::cout << "Number of edges: " << simplified_terrain.number_of_edges() << std::endl;
    std::cout << "Number of faces: " << simplified_terrain.number_of_faces() << std::endl;
    std::cout << "Number of unbounded faces: " << simplified_terrain.number_of_unbounded_faces() << std::endl;
    std::cout << "Number of critical points: " << count_critical_points(simplified_terrain,false) << std::endl;

    
    for(Arrangement::Vertex_iterator vit = simplified_terrain.vertices_begin();
	vit!=simplified_terrain.vertices_end();
	vit++) {
      CGAL_assertion(vit->data().hint.is<Vertex_const_handle>() || vit->data().hint.is<Halfedge_const_handle>() || vit->data().hint.is<Face_const_handle>());
    }

    std::cout << "Is triang: " << is_triangulation(simplified_terrain) << std::endl;
    
    write_to_obj_file(simplified_terrain,input_filename,"blw");
    if(vm.count("only-write-simplified-obj-file")) {
      return 0;
    }

  }

  if(use_simplification) {
    Persistence_diagram pers_0,pers_1;
    set_lower_star_data(simplified_terrain);
    compute_persistence(simplified_terrain,pers_0,pers_1);
    std::cout << "Number of points in pers 0: " << pers_0.size() << std::endl;
    std::cout << "Number of points in pers 1: " << pers_1.size() << std::endl;
    bool no_pers_smaller_2delta=true;
    for(int i=0;i<pers_0.size();i++) {
      if(pers_0[i].second-pers_0[i].first<=2*delta) {
	std::cout << "Found persistence pair in dim 0: " << pers_0[i].first << " " << pers_0[i].second << " 2delta=" << 2*delta << std::endl;
	no_pers_smaller_2delta=false;
	// Avoid long output
	//break;
      }
    }
    for(int i=0;i<pers_1.size();i++) {
      if(pers_1[i].second-pers_1[i].first<=2*delta) {
	std::cout << "Found persistence pair in dim 1: " << pers_1[i].first << " " << pers_1[i].second << " 2delta=" << 2*delta <<std::endl;
	no_pers_smaller_2delta=false;
	// Avoid long output
	//break;
      }
    }
    if(no_pers_smaller_2delta) {
      std::cout << "No persistence smaller than 2delta found" << std::endl;
      
    }
  }

  
  /*
  {
    std::cout << "Now compute L_infty distance" << std::endl;
    Ric_point_location point_location_simplified(simplified_terrain);
    NT dist_bound = L_infty_distance_terrains(simplified_terrain,
					   input_terrain,
					   point_location_simplified,
					   point_location_input);
    std::cout << "The L_infty distance is " << dist_bound << std::endl;  
  }
  */

  algorithm_timer.resume();
  reduction_timer.start();

  Link_retriangulator retriangulator(input_terrain);
  retriangulator.set_flags(mind_persistent_aware,
			   keep_critical_points,
			   mind_L_infty_on_edges,
			   mind_L_infty_on_triangles);
  
  if(use_heap_simplifier) {

    retriangulator.set_L_infty_threshold(delta);

    Heap_simplifier simplifier(simplified_terrain,retriangulator);
    std::cout << "Initialize heap..." << std::endl;
    simplifier.initialize_heap();

    int elements_removed=0;
    progress_timer.start();
    while(!simplifier.empty() && simplifier.peek_min_value()<=delta) {
      
      NT next_bound = simplifier.remove_next_vertex();
      elements_removed++;
      //dist_bound = std::max(dist_bound,next_bound);
      if(elements_removed%print_progress==0) {
	std::cout << "Removed " << elements_removed << " elements (" << simplified_terrain.number_of_vertices() << " remaining)" << " Time for this iteration: " << double(progress_timer.elapsed().wall)/std::pow(10,9) << ", number of checks " << simplifier.count_retriang << std::endl;
	progress_timer.stop();
	progress_timer.start();
	simplifier.count_retriang=0;
      }
    }

  } else {
    retriangulator.set_L_infty_threshold(delta);
    Greedy_simplifier simplifier(simplified_terrain,retriangulator);
    int elements_removed=0;
    progress_timer.start();
    while(simplifier.remove_next_vertex()) {
      elements_removed++;
      if(elements_removed%print_progress==0) {
	int no_attempts = simplifier.get_number_of_attempts();
	std::cout << "Removed " << elements_removed << " elements (" << simplified_terrain.number_of_vertices() << " remaining)" << " - Tried " << no_attempts << " removals - Time for this iteration: " << double(progress_timer.elapsed().wall)/std::pow(10,9) << " - Normalized " << double(progress_timer.elapsed().wall)/std::pow(10,9)*1000/no_attempts << std::endl;
	progress_timer.stop();
	progress_timer.start();
	simplifier.reset_number_of_attempts();
      }
    }
    std::cout << "Vertex removal done, removed " << elements_removed << " vertices" << std::endl;
  }
  
  reduction_timer.stop();


  if(do_flips) {

    write_to_obj_file(simplified_terrain,input_filename,suffix_for_simplified_terrain(simplified_terrain,delta,"no_flip"));
    flip_timer.start();
    std::cout << "Flip edges" << std::endl;
    std::cout << simplified_terrain.number_of_edges() << " edges in total" << std::endl;
    Edge_checker checker(input_terrain);

    Greedy_edge_flipper flipper(simplified_terrain,checker,delta);
    int edges_flipped=0;
    while(flipper.flip_next_edge()) {
      edges_flipped++;
      if(edges_flipped%print_progress==0) {
	std::cout << "Flipped " << edges_flipped << " edges" << std::endl;
      }
    }
    std::cout << "Edge flipping done, flipped " << edges_flipped << " edges" << std::endl;
    for(auto eit = simplified_terrain.edges_begin();eit!=simplified_terrain.edges_end();eit++) {
      Halfedge_handle eh (eit);
      CGAL_assertion(! checker(eh,delta));
    }
    flip_timer.stop();
  }

  algorithm_timer.stop();

  print_timers();

  std::cout << "Is final output a triangulation? " << (is_triangulation(simplified_terrain) ? "Yes" : "No" ) << std::endl;
  std::cout << "Finished with " << std::endl;
  std::cout << "Number of vertices: " << simplified_terrain.number_of_vertices() << std::endl;
  std::cout << "Number of edges: " << simplified_terrain.number_of_edges() << std::endl;
  std::cout << "Number of faces: " << simplified_terrain.number_of_faces() << std::endl;
  std::cout << "Number of unbounded faces: " << simplified_terrain.number_of_unbounded_faces() << std::endl;
  std::cout << "Number of critical points: " << count_critical_points(simplified_terrain) << std::endl;


  std::cout << "Writing obj file" << std::endl;
  write_to_obj_file(simplified_terrain,input_filename,suffix_for_simplified_terrain(simplified_terrain,delta,do_flips ? "with_flip" : ""));
  std::cout << "Compute final L_infty distance" << std::endl;
  Ric_point_location point_location_simplified(simplified_terrain);
  NT L_infty_dist = L_infty_distance_terrains(simplified_terrain,input_terrain,point_location_simplified,point_location_input);
  std::cout << "The L_infty distance is " << L_infty_dist << std::endl;
  
  if(use_simplification) {
    Persistence_diagram pers_0,pers_1;
    set_lower_star_data(simplified_terrain);
    compute_persistence(simplified_terrain,pers_0,pers_1);
    std::cout << "Number of points in pers 0: " << pers_0.size() << std::endl;
    std::cout << "Number of points in pers 1: " << pers_1.size() << std::endl;
    bool no_pers_smaller_2delta=true;
    for(int i=0;i<pers_0.size();i++) {
      if(pers_0[i].second-pers_0[i].first<=2*delta) {
	std::cout << "Found persistence pair: " << pers_0[i].first << " " << pers_0[i].second << std::endl;
	no_pers_smaller_2delta=false;
	// Avoid long output
	//break;
      }
    }
    for(int i=0;i<pers_1.size();i++) {
      if(pers_1[i].second-pers_1[i].first<=2*delta) {
	std::cout << "Found persistence pair: " << pers_1[i].first << " " << pers_1[i].second << std::endl;
	no_pers_smaller_2delta=false;
	// Avoid long output
	//break;
      }
    }
    if(no_pers_smaller_2delta) {
      std::cout << "No persistence smaller than 2delta found" << std::endl;
    }
  }

  std::cout << "Good bye" << std::endl;

  return 0;
}
